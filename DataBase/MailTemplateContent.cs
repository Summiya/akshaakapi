//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class MailTemplateContent
    {
        public string mailerCode { get; set; }
        public string subjectEN { get; set; }
        public string bodyEN { get; set; }
        public string smsEN { get; set; }
        public string subjectAR { get; set; }
        public string bodyAR { get; set; }
        public string smsAR { get; set; }
    }
}
