//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class Discount
    {
        public System.Guid disID { get; set; }
        public string disName { get; set; }
        public string disType { get; set; }
        public Nullable<double> disValue { get; set; }
        public Nullable<System.DateTime> disCreatedOn { get; set; }
        public string disCreatedBy { get; set; }
        public Nullable<System.DateTime> disModifiedOn { get; set; }
        public string disModifiedBy { get; set; }
        public Nullable<bool> disIsDeleted { get; set; }
        public Nullable<bool> disIsActive { get; set; }
    }
}
