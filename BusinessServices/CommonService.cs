﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericRepository;
using UnitOfWork;
using Interfaces;
using BusinessEntities;
using System.Data.SqlClient;
using DataBase;
using AutoMapper;
using MapperUtility;

namespace BusinessServices
{
    class CommonService : ICommon
    {

        private readonly UnitOfWork.UnitOfWork _unitOfWork;

        public CommonService(UnitOfWork.UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public List<CityAreaEntity> GetAllAreasByCity(Guid CityID)
        {
            var areas = _unitOfWork.CityAreaRepository.GetMany(c => c.CityID == CityID);
            List<CityAreaEntity> arealist = Mapper.Map<List<CityAreaEntity>>(areas);
            return arealist;
        }

        public List<CityEntity> GetAllCities()
        {
            var cities = _unitOfWork.CityRepository.GetAll();
            List<CityEntity> citylist = Mapper.Map<List<CityEntity>>(cities);
            return citylist;
        }

        public List<CommonEntity> GetAllCountries()
        {
            var country = _unitOfWork.CountryRepository.GetAll();
            List<CommonEntity> countrylist = Mapper.Map<List<CommonEntity>>(country);
            return countrylist;
        }

        public List<MenuMappingEntity> GetAllMenuMapping(Guid _menuID, Guid rollID)
        {
            throw new NotImplementedException();
        }

        public List<MenuEntity> GetAllMenus()
        {
            throw new NotImplementedException();
        }

        public List<NationalityEntity> GetAllNationality()
        {
            throw new NotImplementedException();
        }

        public List<NGOMasterEntity> GetAllNGOMaster()
        {
            throw new NotImplementedException();
        }

        public List<OfferTypeEntity> GetAllOfferType()
        {
            throw new NotImplementedException();
        }

        public List<SocialMediaMasterEntity> GetAllSocialMediaMaster()
        {
            throw new NotImplementedException();
        }

        public List<WeekDayEntity> GetAllSocialWeekDay()
        {
            throw new NotImplementedException();
        }
    }
}
