﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Interfaces;
using BusinessEntities;

using UnitOfWork;
using DataBase;

using AutoMapper;
using MapperUtility;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace BusinessServices
{
    class LoginService : ILogin
    {
        private readonly UnitOfWork.UnitOfWork _unitOfWork;

        public LoginService(UnitOfWork.UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Delete(Guid Id)
        {
            throw new NotImplementedException();
        }

        public List<LoginEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null)
        {
            throw new NotImplementedException();
        }

        public LoginEntity GetRecordByID(Guid Id)
        {
            throw new NotImplementedException();
        }

        public Guid Save(LoginEntity _Entity)
        {
            try
            {
                Login _dbObj = Mapper.Map<Login>(_Entity);

                if (_Entity.cusLogID == Guid.Empty)
                {
                    _dbObj.cusLogCreatedOn = DateTime.Now;
                    _dbObj.cusLogID = Guid.NewGuid();

                    _unitOfWork.LoginRepository.Insert(_dbObj);
                }
                else
                {
                    _dbObj.cusLogModifiedOn = DateTime.Now;

                    _unitOfWork.LoginRepository.UpdateEntity(_dbObj, _Entity.cusLogID);

                }

                _unitOfWork.Save();
                return _dbObj.cusLogID;

            }

            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }

        }

        public LoginEntity Update(LoginEntity _Entity)
        {
            throw new NotImplementedException();
        }
    }

}
