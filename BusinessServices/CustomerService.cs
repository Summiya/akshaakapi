﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericRepository;
using UnitOfWork;
using Interfaces;
using BusinessEntities;
using System.Data.SqlClient;
using DataBase;
using AutoMapper;
using MapperUtility;

namespace BusinessServices
{
    class CustomerService : ICustomer 
    {
        private readonly UnitOfWork.UnitOfWork _unitOfWork;

        public CustomerService(UnitOfWork.UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool CheckEmailIfExist(string cusLogEmail)
        {
            throw new NotImplementedException();
        }

        public void Delete(Guid Id)
        {
            throw new NotImplementedException();
        }

        public List<CustomerEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null)
        {
            throw new NotImplementedException();
        }

        public CustomerEntity GetRecordByID(Guid Id)
        {
            throw new NotImplementedException();
        }

        public Guid Save(CustomerEntity _Entity)
        {
            throw new NotImplementedException();
        }

        public AttributeEntity Update(CustomerEntity _Entity)
        {
            throw new NotImplementedException();
        }
    }
}
