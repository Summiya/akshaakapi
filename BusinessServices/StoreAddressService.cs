﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericRepository;
using UnitOfWork;
using Interfaces;
using BusinessEntities;
using System.Data.SqlClient;
using DataBase;
using AutoMapper;
using MapperUtility;

namespace BusinessServices
{
    class StoreAddressService : IStoreAddress
    {

        private readonly UnitOfWork.UnitOfWork _unitOfWork;

        public StoreAddressService(UnitOfWork.UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public void Save(StoreAddressEntity storeaddressEntity)
        {
           
            StoreAddress _storeaddress = Mapper.Map<StoreAddress>(storeaddressEntity);

            if (storeaddressEntity.stoAddID == Guid.Empty)
            {
                _storeaddress.stoAddCreatedOn = DateTime.Now;
                _storeaddress.stoAddID = Guid.NewGuid();

                _unitOfWork.StoreAddressRepository.Insert(_storeaddress);
            }
            else
            {
                _storeaddress.stoAddModifiedOn = DateTime.Now;

                _unitOfWork.StoreAddressRepository.UpdateEntity(_storeaddress, storeaddressEntity.stoAddID);

            }

            _unitOfWork.Save();
           

        }

      
    }
}
