﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericRepository;
using UnitOfWork;
using Interfaces;
using BusinessEntities;
using System.Data.SqlClient;
using DataBase;
using AutoMapper;
using MapperUtility;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace BusinessServices
{
    class StoreTimingService :IStoreTiming
    {

        private readonly UnitOfWork.UnitOfWork _unitOfWork;

        public StoreTimingService(UnitOfWork.UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Save(StoreTimingEntity strTiming)
        {
            try
            {

                StoreTiming _storetiming = Mapper.Map<StoreTiming>(strTiming);
                _unitOfWork.StoreTimingRepository.Insert(_storetiming);
                _unitOfWork.Save();
            }
            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }

        }
        
        public void Delete(Guid storeID)
        {
            try
            {
                _unitOfWork.StoreTimingRepository.Delete(storeID);
                _unitOfWork.Save();
            }
            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }
        }

       
    }
}
