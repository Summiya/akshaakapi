﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericRepository;
using UnitOfWork;
using Interfaces;
using BusinessEntities;
using System.Data.SqlClient;
using DataBase;
using AutoMapper;
using MapperUtility;

namespace BusinessServices
{
    class CartWhishlistService : ICartWhishlist
    {
        public void Delete(Guid Id)
        {
            throw new NotImplementedException();
        }

        public List<CartWhishlistEntity> GetAllCartWishList(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null)
        {
            throw new NotImplementedException();
        }

        public List<CartWhishlistEntity> GetAllCartWishListByCustomer(Guid customerID)
        {
            throw new NotImplementedException();
        }

        public CartWhishlistEntity GetCartWishlist(Guid Id)
        {
            throw new NotImplementedException();
        }

        public Guid Save(CartWhishlistEntity _Entity)
        {
            throw new NotImplementedException();
        }

        public CartWhishlistEntity Update(CartWhishlistEntity _Entity)
        {
            throw new NotImplementedException();
        }
    }
}
