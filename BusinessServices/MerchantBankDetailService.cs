﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GenericRepository;
using UnitOfWork;
using Interfaces;
using BusinessEntities;
using System.Data.SqlClient;
using DataBase;
using AutoMapper;
using MapperUtility;

namespace BusinessServices
{
    class MerchantBankDetailService : IMerchantBankDetail
    {

        private readonly UnitOfWork.UnitOfWork _unitOfWork;

        public MerchantBankDetailService(UnitOfWork.UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        public void Save(MerchantBankDetailEntity _Entity)
        {           
            
            MerchantBankDetail _mBankDetail = Mapper.Map<MerchantBankDetail>(_Entity);

            if (_Entity.merBanDetID == Guid.Empty)
            {
                _mBankDetail.merBanDetCreatedOn = DateTime.Now;
                _mBankDetail.merBanDetID = Guid.NewGuid();

                _unitOfWork.MerchantBankDetailRepository.Insert(_mBankDetail);
            }
            else
            {
                _mBankDetail.merBanDetModifiedOn = DateTime.Now;

                _unitOfWork.MerchantBankDetailRepository.UpdateEntity(_mBankDetail, _Entity.merBanDetID);

            }

            _unitOfWork.Save();
           

        }

      
    }
}
