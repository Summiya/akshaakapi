﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericRepository;
using UnitOfWork;
using Interfaces;
using BusinessEntities;
using System.Data.SqlClient;
using DataBase;
using AutoMapper;
using MapperUtility;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace BusinessServices
{
    class StoreService : IStore
    {

        private readonly UnitOfWork.UnitOfWork _unitOfWork;

        public StoreService(UnitOfWork.UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public StoreEntity GetStorebyStoreID(Guid Id)
        {
            Store StoreDetail = _unitOfWork.StoreRepository.GetByID(Id);
            StoreEntity _storeEntity = Mapper.Map<StoreEntity>(StoreDetail);
            return _storeEntity;

        }


        public Guid Save(StoreEntity _storeEntity)
        {

            try
            {
                Store _store = Mapper.Map<Store>(_storeEntity);

                if (_storeEntity.stoID == Guid.Empty)
                {
                    _store.stoCreatedOn = DateTime.Now;
                    _store.stoID = Guid.NewGuid();

                    _unitOfWork.StoreRepository.Insert(_store);
                }
                else
                {
                    _store.stoModifiedOn = DateTime.Now.ToString();

                    _unitOfWork.StoreRepository.UpdateEntity(_store, _storeEntity.stoID);

                }

                _unitOfWork.Save();
                return _store.stoID;

            }
            
            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }
            
        }
        
        public StoreEntity GetStorebyMerchantID(Guid merchantID)
        {
            throw new NotImplementedException();
        }
    }
}
