﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;
using Interfaces;

using AutoMapper;
using MapperUtility;

using DataBase;

using Constant;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace BusinessServices
{
    class MerchantService : IMerchant
    {
        private readonly UnitOfWork.UnitOfWork _unitOfWork;

        public MerchantService(UnitOfWork.UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public void Delete(Guid Id)
        {
            throw new NotImplementedException();
        }

        public List<MerchantEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null)
        {
            var result = _unitOfWork.MerchantRepository.GetAllRecords(SearchValue, PageNo, PageSize, SortColumn, SortOrder);
            List<MerchantEntity> _merchantentity = Mapper.Map<List<MerchantEntity>>(result);
            return _merchantentity;
        }

        public MerchantEntity GetRecordByID(Guid Id)
        {
            var result = _unitOfWork.MerchantRepository.GetRecordByID(Id);
            MerchantEntity _merchantentity = Mapper.Map<MerchantEntity>(result);
            return _merchantentity;
        }

        public bool ISMerchantExist(string email)
        {            
            var result = _unitOfWork.LoginRepository.Query(x => x.cusLogEmail == email &&  x.cusLogType == Constant.MERCHANT ).ToList();
            if (result.Count > 0)
                return true;
            else return false;
        }

        public Guid Save(MerchantEntity _Entity)
        {
            try
            {

                Merchant _dbObj = Mapper.Map<Merchant>(_Entity);
                _unitOfWork.MerchantRepository.Add(_dbObj);
                _unitOfWork.Save();

               
                return _dbObj.merID;

            }
            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }
        }

        public MerchantEntity Update(MerchantEntity _Entity)
        {
            throw new NotImplementedException();
        }
    }
}
