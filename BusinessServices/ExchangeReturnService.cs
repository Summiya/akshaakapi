﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;
using Interfaces;

namespace BusinessServices  
{
    class ExchangeReturnService : IExchangeReturnPol, IExchangeReturnTimeEntity
    {
        public void Delete(Guid Id)
        {
            throw new NotImplementedException();
        }

        public List<ExchangeReturnTimeEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null)
        {
            throw new NotImplementedException();
        }

        public ExchangeReturnTimeEntity GetRecordByID(Guid Id)
        {
            throw new NotImplementedException();
        }

        public Guid Save(ExchangeReturnTimeEntity _Entity)
        {
            throw new NotImplementedException();
        }

        public ExchangeReturnTimeEntity Update(ExchangeReturnTimeEntity _Entity)
        {
            throw new NotImplementedException();
        }

        void IExchangeReturnPol.Delete(Guid Id)
        {
            throw new NotImplementedException();
        }

        List<ExchangeReturnPolEntity> IExchangeReturnPol.GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn, string SortOrder)
        {
            throw new NotImplementedException();
        }

        ExchangeReturnPolEntity IExchangeReturnPol.GetRecordByID(Guid Id)
        {
            throw new NotImplementedException();
        }

        Guid IExchangeReturnPol.Save(ExchangeReturnPolEntity _Entity)
        {
            throw new NotImplementedException();
        }

        AttributeEntity IExchangeReturnPol.Update(ExchangeReturnPolEntity _Entity)
        {
            throw new NotImplementedException();
        }
    }
}
