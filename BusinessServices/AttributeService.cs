﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericRepository;
using UnitOfWork;
using Interfaces;
using BusinessEntities;
using System.Data.SqlClient;
using DataBase;
using AutoMapper;
using MapperUtility;

namespace BusinessServices
{
    class AttributeService : IAttribute
    {
        public void DeleteAttributebyId(Guid Id)
        {
            throw new NotImplementedException();
        }

        public void DeleteAttributeOptionbyId(Guid Id)
        {
            throw new NotImplementedException();
        }

        public List<AttributeEntity> GetAllAttributes(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null)
        {
            throw new NotImplementedException();
        }

        public List<AttributesOptionEntity> GetAllAttributesOptions(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null)
        {
            throw new NotImplementedException();
        }

        public AttributeEntity GetAttributebyID(Guid Id)
        {
            throw new NotImplementedException();
        }

        public AttributesOptionEntity GetAttributeOptionbyID(Guid Id)
        {
            throw new NotImplementedException();
        }

        public Guid SaveAttribute(AttributeEntity _attributeEntity)
        {
            throw new NotImplementedException();
        }

        public Guid SaveAttributeOptions(AttributesOptionEntity _attributeEntity)
        {
            throw new NotImplementedException();
        }

        public AttributeEntity UpdateAttribute(AttributeEntity _attributeEntity)
        {
            throw new NotImplementedException();
        }

        public AttributesOptionEntity UpdateAttributeOptionste(AttributesOptionEntity _attributeEntity)
        {
            throw new NotImplementedException();
        }
    }
}
