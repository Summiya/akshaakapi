﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
   public class StoreEntity
    {
        public System.Guid stoID { get; set; }
        public string merID { get; set; }
        public string curID { get; set; }
        public string couID { get; set; }
        public string shoCatID { get; set; }
        public string lanID { get; set; }
        public string lanIDArb { get; set; }
        public string stoName { get; set; }
        public string stoDescription { get; set; }
        public string stoArbName { get; set; }
        public string stoArbDescription { get; set; }
        public string stoEmailID { get; set; }
        public string stoContactNo { get; set; }
        public string stoOwnerName { get; set; }
        public string stoImgName { get; set; }
        public string stoLogo { get; set; }
        public string stoBgImgName { get; set; }
        public string stoAdminEmails { get; set; }
        public string stoAdminCCEmails { get; set; }
        public string stoSMTPEmail { get; set; }
        public string stoSMTPPassword { get; set; }
        public Nullable<int> stoSMTPPort { get; set; }
        public Nullable<bool> stoSMTPSSL { get; set; }
        public string stoSeoName { get; set; }
        public string stoLink { get; set; }
        public Nullable<bool> stoApproved { get; set; }
        public Nullable<bool> stoPublished { get; set; }
        public Nullable<bool> stoInventory { get; set; }
        public string stoTimeZone { get; set; }
        public string stoMetaKeyword { get; set; }
        public string stoMetaDescription { get; set; }
        public Nullable<System.DateTime> stoCreatedOn { get; set; }
        public string stoCreatedBy { get; set; }
        public string stoModifiedOn { get; set; }
        public string stoModifiedBy { get; set; }
        public Nullable<bool> stoIsDeleted { get; set; }
        public Nullable<bool> stoIsActive { get; set; }
        public Nullable<bool> stoIsClosed { get; set; }
        public string stoShopPolicies { get; set; }
        public Nullable<double> stoDeliveryCharges { get; set; }
        public string stoShopStatus { get; set; }
        public Nullable<System.DateTime> stoStartDate { get; set; }
        public Nullable<System.DateTime> stoEndDate { get; set; }
        public string stoStartTime { get; set; }
        public string stoEndTime { get; set; }
        public Nullable<bool> stoIsAcceptOrder { get; set; }
        public Nullable<bool> stoCheckoutdisable { get; set; }
        public string ngoID { get; set; }

        public bool Myshop { get; set; }
        public bool lcoationSetup { get; set; }
        public bool ShopSetup { get; set; }

        public StoreAddressEntity ObjStoreAddress { get; set; }
        public StoreTimingEntity ObjStoreTiming { get; set; }

        public MerchantBankDetailEntity ObjMerchantBankDetail { get; set; }
    }


    public class CategoryMappingEntity
    {
        public System.Guid catMapID { get; set; }
        public string catID { get; set; }
        public string stoID { get; set; }
        public Nullable<System.DateTime> catMapCreatedOn { get; set; }
        public string catMapCreatedBy { get; set; }
        public Nullable<System.DateTime> catMapModifiedOn { get; set; }
        public string catMapModifiedBy { get; set; }
        public Nullable<bool> catMapIsDeleted { get; set; }
        public Nullable<bool> catMapIsActive { get; set; }
    }

    public class SocialMediaEntity
    {
        public System.Guid socMedID { get; set; }
        public string socMedMasID { get; set; }
        public string stoID { get; set; }
        public string socMedName { get; set; }
        public string socMedLink { get; set; }
        public Nullable<System.DateTime> socMedCreatedOn { get; set; }
        public string socMedCreatedBy { get; set; }
        public Nullable<System.DateTime> socMedModifiedOn { get; set; }
        public string socMedModifiedBy { get; set; }
        public Nullable<bool> socMedIsDeleted { get; set; }
        public Nullable<bool> socMedIsActive { get; set; }
    }

    public class StoreAddressEntity
    {
        public System.Guid stoAddID { get; set; }
        public string stoID { get; set; }
        public string citID { get; set; }
        public string stoAddAddress { get; set; }
        public Nullable<int> stoBVType { get; set; }
        public string stoAddCity { get; set; }
        public string stoAddState { get; set; }
        public string stoAddCountry { get; set; }
        public string stoAddZipcode { get; set; }
        public string stoAddArea { get; set; }
        public string StoAddBuilding { get; set; }
        public string stoAddAptNo { get; set; }
        public string stoAddLongitude { get; set; }
        public string stoAddLatitude { get; set; }
        public string stoAddType { get; set; }
        public string stoAddStreet { get; set; }
        public string stoAddFloor { get; set; }
        public Nullable<bool> stoAddShowMyLoc { get; set; }
        public Nullable<bool> stoAddAvailablePikup { get; set; }
        public Nullable<System.DateTime> stoAddCreatedOn { get; set; }
        public string stoAddCreatedBy { get; set; }
        public Nullable<System.DateTime> stoAddModifiedOn { get; set; }
        public string stoAddModifiedBy { get; set; }
        public Nullable<bool> stoAddIsDeleted { get; set; }
        public Nullable<bool> stoAddIsActive { get; set; }
        public Nullable<bool> stoPersonalDelAvl { get; set; }
        public string stoPersonalDelCitID { get; set; }
    }

    public  class StoreDeliveryMappingEntity
    {
        public string stoDelMapID { get; set; }
        public string stoID { get; set; }
        public string citID { get; set; }
        public Nullable<double> stoDelMapAmt { get; set; }
        public Nullable<bool> stoDelIsEnabled { get; set; }
    }

    public class StoreFavrotyEntity
    {
        public System.Guid stoFavID { get; set; }
        public string stoID { get; set; }
        public string cusID { get; set; }
        public Nullable<System.DateTime> stoFavCreatedOn { get; set; }
    }

    public class StoreLicenseEntity
    {
        public System.Guid stoLicID { get; set; }
        public string stoID { get; set; }
        public string stoLicEmirateIDName { get; set; }
        public string stoLicEmiratesIDNumber { get; set; }
        public string stoLicEmiratesExpirydate { get; set; }
        public string stoLicETraderLicenseNumber { get; set; }
        public string stoLicETraderLicenseExpirydate { get; set; }
        public string stoLicETraderRegisteredActivity { get; set; }
        public Nullable<System.DateTime> stoLicCreatedOn { get; set; }
        public string stoLicCreatedBy { get; set; }
        public Nullable<System.DateTime> stoLicModifiedOn { get; set; }
        public string stoLicModifiedBy { get; set; }
        public Nullable<bool> stoLicIsActive { get; set; }
        public Nullable<bool> stoLicIsDeleted { get; set; }
    }

    public  class StoreOffreEntity
    {
        public System.Guid stoOffID { get; set; }
        public string stoID { get; set; }
        public string offTypID { get; set; }
        public string proID { get; set; }
        public string stoOffImg { get; set; }
        public string stoOffDesc { get; set; }
        public string stoOffLink { get; set; }
        public string stoOffType { get; set; }
        public string stoOffExtInt { get; set; }
        public string stoOffCode { get; set; }
        public Nullable<System.DateTime> stoOffFromDate { get; set; }
        public Nullable<System.DateTime> stoOffToDate { get; set; }
        public Nullable<double> stoOffDiscount { get; set; }
        public Nullable<double> stoOffMinAmt { get; set; }
        public string stoOffMsg { get; set; }
        public Nullable<int> stoOffOrderCount { get; set; }
        public Nullable<System.DateTime> stoOffCreatedOn { get; set; }
        public string stoOffCreatedBy { get; set; }
        public Nullable<System.DateTime> stoOffModifiedOn { get; set; }
        public string stoOffModifiedBy { get; set; }
        public Nullable<bool> stoOffIsDeleted { get; set; }
        public Nullable<bool> stoOffIsActive { get; set; }
    }

    public class StorePaymentTypeEntity
    {
        public System.Guid stoPayTypID { get; set; }
        public string stoID { get; set; }
        public string payTypID { get; set; }
        public Nullable<System.DateTime> stoPayTypCreatedOn { get; set; }
        public string stoPayTypCreatedBy { get; set; }
        public Nullable<System.DateTime> stoPayTypModifiedOn { get; set; }
        public string stoPayTypModifiedBy { get; set; }
        public Nullable<bool> stoPayTypIsDeleted { get; set; }
        public Nullable<bool> stoPayTypIsActive { get; set; }
    }

    public class StoresDiscountEntity
    {
        public System.Guid stoDisID { get; set; }
        public string stoID { get; set; }
        public string stoDisName { get; set; }
        public string stoDisType { get; set; }
        public Nullable<double> stoDisValue { get; set; }
        public Nullable<bool> stoDisIsDefault { get; set; }
        public string stoDisFromDate { get; set; }
        public string stoDisToDate { get; set; }
        public Nullable<System.DateTime> stoDisCreatedOn { get; set; }
        public string stoDisCreatedBy { get; set; }
        public Nullable<System.DateTime> stoDisModifiedOn { get; set; }
        public string stoDisModifiedBy { get; set; }
        public Nullable<bool> stoDisIsDeleted { get; set; }
        public Nullable<bool> stoDisIsActive { get; set; }
    }

    public class StoresGallaryEntity
    {
        public System.Guid stoGalID { get; set; }
        public string stoID { get; set; }
        public string stoGalImgName { get; set; }
        public Nullable<System.DateTime> stoGalCreatedOn { get; set; }
        public string stoGalCreatedBy { get; set; }
        public Nullable<System.DateTime> stoGalModifiedOn { get; set; }
        public string stoGalModifiedBy { get; set; }
        public Nullable<bool> stoGalIsDeleted { get; set; }
        public Nullable<bool> stoGalIsActive { get; set; }
    }


    public  class StoresTaxEntity
    {
        public System.Guid stoTaxID { get; set; }
        public string stoID { get; set; }
        public string stoTaxName { get; set; }
        public string stoTaxType { get; set; }
        public Nullable<double> stoTaxValue { get; set; }
        public Nullable<bool> stoTaxIsDefault { get; set; }
        public Nullable<System.DateTime> stoTaxCreatedOn { get; set; }
        public string stoTaxCreatedBy { get; set; }
        public Nullable<System.DateTime> stoTaxModifiedOn { get; set; }
        public string stoTaxModifiedBy { get; set; }
        public Nullable<bool> stoTaxIsDeleted { get; set; }
        public Nullable<bool> stoTaxIsActive { get; set; }
    }

    public partial class StoreTimingEntity
    {
        public System.Guid stoTimID { get; set; }
        public string stoID { get; set; }
        public string stoTimDayName { get; set; }
        public string stoTimOpeningTime { get; set; }
        public string stoTimClosingTime { get; set; }
        public Nullable<System.DateTime> stoTimCreatedOn { get; set; }
        public string stoTimCreatedBy { get; set; }
        public Nullable<System.DateTime> stoTimModifiedOn { get; set; }
        public string stoTimModifiedBy { get; set; }
        public Nullable<bool> stoTimIsDeleted { get; set; }
        public Nullable<bool> stoTimIsActive { get; set; }
    }


}
