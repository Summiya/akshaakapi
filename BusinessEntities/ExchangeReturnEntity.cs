﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class ExchangeReturnPolEntity
    {
        public System.Guid excRetPolID { get; set; }
        public string excRetPolName { get; set; }
        public string excRetPolNameAr { get; set; }
        public Nullable<System.DateTime> excRetPolCreatedOn { get; set; }
        public string excRetPolCreatedBy { get; set; }
        public Nullable<System.DateTime> excRetPolModifiedOn { get; set; }
        public string excRetPolModifiedBy { get; set; }
        public Nullable<bool> excRetPolIsDeleted { get; set; }
        public Nullable<bool> excRetPolIsActive { get; set; }
    }

    public class ExchangeReturnTimeEntity
    {
        public System.Guid excRetTimID { get; set; }
        public string excRetPolID { get; set; }
        public string excRetTimName { get; set; }
        public string excRetTimNameAr { get; set; }
        public Nullable<int> excRetTimDays { get; set; }
        public Nullable<System.DateTime> excRetTimCreatedOn { get; set; }
        public string excRetTimCreatedBy { get; set; }
        public Nullable<System.DateTime> excRetTimModifiedOn { get; set; }
        public string excRetTimModifiedBy { get; set; }
        public Nullable<bool> excRetTimIsDeleted { get; set; }
        public Nullable<bool> excRetTimIsActive { get; set; }
    }
}
