﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class AttributeEntity
    {
        public System.Guid attID { get; set; }
        public string attName { get; set; }
        public string attDescription { get; set; }
        public string attImg { get; set; }
        public Nullable<int> attPosition { get; set; }
        public string attSeoName { get; set; }
        public Nullable<System.DateTime> attCreatedOn { get; set; }
        public string attCreatedBy { get; set; }
        public Nullable<System.DateTime> attModifiedOn { get; set; }
        public string attModifiedBy { get; set; }
        public Nullable<bool> attIsDeleted { get; set; }
        public Nullable<bool> attIsActive { get; set; }
    }


    public class AttributesOptionEntity
    {
        public System.Guid attOptID { get; set; }
        public string attID { get; set; }
        public string attOptName { get; set; }
        public string attOptDescription { get; set; }
        public string attOptImg { get; set; }
        public Nullable<bool> attOptQuantity { get; set; }
        public Nullable<bool> attOptPrice { get; set; }
        public Nullable<int> attOptPosition { get; set; }
        public string attOptSeoName { get; set; }
        public string attOptType { get; set; }
        public Nullable<System.DateTime> attOptCreatedOn { get; set; }
        public string attOptCreatedBy { get; set; }
        public Nullable<System.DateTime> attOptModifiedOn { get; set; }
        public string attOptModifiedBy { get; set; }
        public Nullable<bool> attOptIsDeleted { get; set; }
        public Nullable<bool> attOptIsActive { get; set; }
    }

}
