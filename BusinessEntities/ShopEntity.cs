﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class ShopCategoryEntity
    {
        public System.Guid shoCatID { get; set; }
        public string shoCatName { get; set; }
        public string shoCatNameAr { get; set; }
        public string shoCatDescription { get; set; }
        public string shoCatDescriptionAr { get; set; }
        public string shoCatImg { get; set; }
        public string shoCatParentID { get; set; }
        public Nullable<int> shoCatPosition { get; set; }
        public Nullable<int> shoCatLevel { get; set; }
        public Nullable<int> shoCatChildrenCount { get; set; }
        public string shoCatSeoName { get; set; }
        public string shoCatIcon { get; set; }
        public Nullable<System.DateTime> shoCatCreatedOn { get; set; }
        public string shoCatCreatedBy { get; set; }
        public Nullable<System.DateTime> shoCatModifiedOn { get; set; }
        public string shoCatModifiedBy { get; set; }
        public Nullable<bool> shoCatIsDeleted { get; set; }
        public Nullable<bool> shoCatIsActive { get; set; }
    }

    public  class ShopCategoryMappingEntity
    {
        public System.Guid shoCatMapID { get; set; }
        public string shoCatID { get; set; }
        public string stoID { get; set; }
        public Nullable<System.DateTime> shoCatMapCreatedOn { get; set; }
        public string shoCatMapCreatedBy { get; set; }
        public Nullable<System.DateTime> shoCatMapModifiedOn { get; set; }
        public string shoCatMapModifiedBy { get; set; }
        public Nullable<bool> shoCatMapIsDeleted { get; set; }
        public Nullable<bool> shoCatMapIsActive { get; set; }
    }
}
