﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
   public  class DeliveryDetailEntity
    {
        public int cDeliveryID { get; set; }
        public string cdelOrderID { get; set; }
        public string cdelOrderdetID { get; set; }
        public string cdelStoreID { get; set; }
        public string cdelStatus { get; set; }
        public string cdelCarryID { get; set; }
        public string cdelError { get; set; }
        public string cdelCode { get; set; }
        public Nullable<System.DateTime> cdelCreatedDate { get; set; }
        public string cdelCreatedBy { get; set; }
    }
}
