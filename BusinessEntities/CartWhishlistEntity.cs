﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class CartWhishlistEntity
    {
        public System.Guid carWhiFavID { get; set; }
        public string cusID { get; set; }
        public string proID { get; set; }
        public string invVarID { get; set; }
        public string carWhiFavQty { get; set; }
        public string carWhiFavType { get; set; }
        public Nullable<int> carDeliveryType { get; set; }
        public Nullable<System.DateTime> cusCarInfCreatedOn { get; set; }
        public string cusCarInfCreatedBy { get; set; }
    }
}
