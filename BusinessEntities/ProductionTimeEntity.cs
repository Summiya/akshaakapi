﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
   public class ProductionTimeEntity
    {
            public System.Guid prodTimID { get; set; }
            public string prodTimName { get; set; }
            public string prodTimNameAr { get; set; }
            public Nullable<int> prodTimDays { get; set; }
            public Nullable<System.DateTime> prodTimCreatedOn { get; set; }
            public string prodTimCreatedBy { get; set; }
            public Nullable<System.DateTime> prodTimModifiedOn { get; set; }
            public string prodTimModifiedBy { get; set; }
            public Nullable<bool> prodTimIsDeleted { get; set; }
            public Nullable<bool> prodTimIsActive { get; set; }
        
    }
}
