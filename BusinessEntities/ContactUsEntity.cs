﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
   public class ContactUsEntity
    {
        public string contactUsID { get; set; }
        public string contactUsName { get; set; }
        public string contactUsEmail1 { get; set; }
        public string contactUsMobileNo { get; set; }
        public string contactUsMessage { get; set; }
        public Nullable<System.DateTime> contactUsCreatedDate { get; set; }
    }
}
