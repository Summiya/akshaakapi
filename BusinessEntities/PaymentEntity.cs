﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class PaymentsInfoEntity
    {
        public System.Guid payInfID { get; set; }
        public string merID { get; set; }
        public string payTypID { get; set; }
        public string payInfTransactionNo { get; set; }
        public string payInfTransactionStatus { get; set; }
        public Nullable<System.DateTime> payInfDate { get; set; }
        public Nullable<double> payInfPrice { get; set; }
        public Nullable<double> payInfTaxAmount { get; set; }
        public Nullable<double> payInfDisAmount { get; set; }
        public Nullable<double> payInfFinalPrice { get; set; }
        public Nullable<System.DateTime> payInfCreatedOn { get; set; }
        public string payinfCreatedBy { get; set; }
        public Nullable<System.DateTime> payInfModifiedOn { get; set; }
        public string payInfModifiedBy { get; set; }
        public Nullable<bool> payInfIsDeleted { get; set; }
        public Nullable<bool> payInfIsActive { get; set; }
    }

    public class PaymentTypeEntity
    {
        public System.Guid payTypID { get; set; }
        public string payTypName { get; set; }
        public string payTypNameAr { get; set; }
        public string payTypDescription { get; set; }
        public string payTypDescriptionAr { get; set; }
        public string payTypIcon { get; set; }
        public Nullable<System.DateTime> payTypCreatedOn { get; set; }
        public string payTypCreatedBy { get; set; }
        public Nullable<System.DateTime> payTypModifiedOn { get; set; }
        public string payTypModifiedBy { get; set; }
        public Nullable<bool> payTypIsDeleted { get; set; }
        public Nullable<bool> payTypIsActive { get; set; }
    }
}
