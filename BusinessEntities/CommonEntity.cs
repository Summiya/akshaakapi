﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class CommonEntity
    {
        public System.Guid couID { get; set; }
        public string couCode { get; set; }
        public string couName { get; set; }
        public string couNameAr { get; set; }
        public string couIcon { get; set; }
        public Nullable<System.DateTime> couCreatedOn { get; set; }
        public string couCreatedBy { get; set; }
        public Nullable<System.DateTime> couModifiedOn { get; set; }
        public string couModifiedBy { get; set; }
        public Nullable<bool> couIsDeleted { get; set; }
        public Nullable<bool> couIsActive { get; set; }
    }

    public class CityEntity
    {
        public System.Guid citID { get; set; }
        public string staID { get; set; }
        public string citName { get; set; }
        public string citNameAr { get; set; }
        public Nullable<System.DateTime> citCreatedOn { get; set; }
        public string citCreatedBy { get; set; }
        public Nullable<System.DateTime> citModifiedOn { get; set; }
        public string citModifiedBy { get; set; }
        public Nullable<bool> citIsDeleted { get; set; }
        public Nullable<bool> citIsActive { get; set; }
    }

    public class CityAreaEntity
    {
        public System.Guid AddressID { get; set; }
        public Nullable<System.Guid> CityID { get; set; }
        public string AddrAreaNameEn { get; set; }
        public string AddrAreaNameAr { get; set; }
    }

    public class CurrencyEntity
    {
        public System.Guid curID { get; set; }
        public string curCountryName { get; set; }
        public string curCountryNameAr { get; set; }
        public string curCode { get; set; }
        public string curName { get; set; }
        public string curNameAr { get; set; }
        public string curIcon { get; set; }
        public Nullable<System.DateTime> curCreatedOn { get; set; }
        public string curCreatedBy { get; set; }
        public Nullable<System.DateTime> curModifiedOn { get; set; }
        public string curModifiedBy { get; set; }
        public Nullable<bool> curIsDeleted { get; set; }
        public Nullable<bool> curIsActive { get; set; }
    }

    public  class MenuEntity
    {
        public System.Guid menID { get; set; }
        public string menName { get; set; }
        public string menLink { get; set; }
        public string menIcon { get; set; }
        public string menParentID { get; set; }
        public Nullable<System.DateTime> menCreatedOn { get; set; }
        public string menCreatedBy { get; set; }
        public Nullable<System.DateTime> menModifiedOn { get; set; }
        public string menModifiedBy { get; set; }
        public Nullable<bool> menIsDeleted { get; set; }
        public Nullable<bool> menIsActive { get; set; }
    }


    public partial class MenuMappingEntity
    {
        public System.Guid menMapID { get; set; }
        public string rolID { get; set; }
        public string menID { get; set; }
        public Nullable<System.DateTime> menMapCreatedOn { get; set; }
        public string menMapCreatedBy { get; set; }
        public Nullable<System.DateTime> menMapModifiedOn { get; set; }
        public string menMapModifiedBy { get; set; }
        public Nullable<bool> menMapIsDeleted { get; set; }
        public Nullable<bool> menMapIsActive { get; set; }
    }

    public class NationalityEntity
    {
        public System.Guid natID { get; set; }
        public string natName { get; set; }
        public string natNameAr { get; set; }
        public Nullable<System.DateTime> natCreatedOn { get; set; }
        public string natCreatedBy { get; set; }
        public Nullable<System.DateTime> natModifiedOn { get; set; }
        public string natModifiedBy { get; set; }
        public Nullable<bool> natIsDeleted { get; set; }
        public Nullable<bool> natIsActive { get; set; }
    }

    public class NGOMasterEntity
    {
        public System.Guid ngoID { get; set; }
        public string ngoName { get; set; }
        public string ngoNameAr { get; set; }
        public string ngoImgName { get; set; }
        public Nullable<System.DateTime> ngoCreatedOn { get; set; }
        public string ngoCreatedBy { get; set; }
        public Nullable<System.DateTime> ngoModifiedOn { get; set; }
        public string ngoModifiedBy { get; set; }
        public Nullable<bool> ngoIsDeleted { get; set; }
        public Nullable<bool> ngoIsActive { get; set; }
    }

    public class OfferTypeEntity
    {
        public System.Guid offTypID { get; set; }
        public string offTypName { get; set; }
        public Nullable<System.DateTime> offTypCreatedOn { get; set; }
        public string offTypCreatedBy { get; set; }
        public Nullable<System.DateTime> offTypModifiedOn { get; set; }
        public string offTypModifiedBy { get; set; }
        public Nullable<bool> offTypIsDeleted { get; set; }
        public Nullable<bool> offTypIsActive { get; set; }
    }

    public class SocialMediaMasterEntity
    {
        public System.Guid socMedID { get; set; }
        public string socMedMasID { get; set; }
        public string stoID { get; set; }
        public string socMedName { get; set; }
        public string socMedLink { get; set; }
        public Nullable<System.DateTime> socMedCreatedOn { get; set; }
        public string socMedCreatedBy { get; set; }
        public Nullable<System.DateTime> socMedModifiedOn { get; set; }
        public string socMedModifiedBy { get; set; }
        public Nullable<bool> socMedIsDeleted { get; set; }
        public Nullable<bool> socMedIsActive { get; set; }
    }

    public class WeekDayEntity
    {
        public System.Guid weeDayID { get; set; }
        public string weeDayName { get; set; }
        public string weeDayNameAr { get; set; }
        public Nullable<System.DateTime> weeDayCreatedOn { get; set; }
        public string weeDayCreatedBy { get; set; }
        public Nullable<System.DateTime> weeDayModifiedOn { get; set; }
        public string weeDayModifiedBy { get; set; }
        public Nullable<bool> weeDayIsDeleted { get; set; }
        public Nullable<bool> weeDayIsActive { get; set; }
        public Nullable<int> weeDaySort { get; set; }
    }
}
