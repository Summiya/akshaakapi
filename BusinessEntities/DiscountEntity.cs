﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class DiscountEntity
    {
        public System.Guid disID { get; set; }
        public string disName { get; set; }
        public string disType { get; set; }
        public Nullable<double> disValue { get; set; }
        public Nullable<System.DateTime> disCreatedOn { get; set; }
        public string disCreatedBy { get; set; }
        public Nullable<System.DateTime> disModifiedOn { get; set; }
        public string disModifiedBy { get; set; }
        public Nullable<bool> disIsDeleted { get; set; }
        public Nullable<bool> disIsActive { get; set; }
    }
}
