﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class TaxEntity
    {
            public System.Guid taxID { get; set; }
            public string taxName { get; set; }
            public string taxType { get; set; }
            public Nullable<double> taxValue { get; set; }
            public Nullable<System.DateTime> taxCreatedOn { get; set; }
            public string taxCreatedBy { get; set; }
            public Nullable<System.DateTime> taxModifiedOn { get; set; }
            public string taxModifiedBy { get; set; }
            public Nullable<bool> taxIsDeleted { get; set; }
            public Nullable<bool> taxIsActive { get; set; }
        
    }
}
