﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class OrderEntity
    {
        public System.Guid ordID { get; set; }
        public string cusID { get; set; }
        public string stoID { get; set; }
        public string cusAddID { get; set; }
        public string payTypID { get; set; }
        public string delMapID { get; set; }
        public string ordNumber { get; set; }
        public Nullable<System.DateTime> ordDatetime { get; set; }
        public string ordStatus { get; set; }
        public string ordDeliveryType { get; set; }
        public string ordDeliveryTime { get; set; }
        public string ordCancelRemark { get; set; }
        public Nullable<double> ordSubTotal { get; set; }
        public Nullable<double> ordTotalDiscount { get; set; }
        public Nullable<double> ordDeliveryCharges { get; set; }
        public Nullable<double> ordTotalTax { get; set; }
        public Nullable<double> ordCreditPayment { get; set; }
        public Nullable<double> ordCashPayment { get; set; }
        public Nullable<double> ordGrandTotal { get; set; }
        public string ordOfferCode { get; set; }
        public string ordPaymentStatus { get; set; }
        public string ordFrom { get; set; }
        public string ordSignature { get; set; }
        public Nullable<decimal> ordTaxPer { get; set; }
        public Nullable<System.DateTime> ordCreatedOn { get; set; }
        public string ordCreatedBy { get; set; }
        public Nullable<System.DateTime> ordModifiedOn { get; set; }
        public string ordModifiedBy { get; set; }
        public Nullable<bool> ordIsDeleted { get; set; }
        public Nullable<bool> ordIsActive { get; set; }
    }

    public class OrderDetailEntity
    {
        public System.Guid ordDetID { get; set; }
        public string ordID { get; set; }
        public string proID { get; set; }
        public string invVarID { get; set; }
        public Nullable<double> ordDetPriceOriginalPrice { get; set; }
        public Nullable<double> ordDetPriceOriginalDisPrice { get; set; }
        public Nullable<double> ordDetProPrice { get; set; }
        public Nullable<double> ordDetProQty { get; set; }
        public Nullable<double> ordDetTotalPrice { get; set; }
        public string ordDetWingRefNo { get; set; }
        public Nullable<bool> ordPickUp { get; set; }
        public Nullable<double> ordDetProDiscount { get; set; }
        public string ordDetcusAddrID { get; set; }
        public string ordDetPaymentID { get; set; }
        public Nullable<double> ordDetSubTotal { get; set; }
        public Nullable<double> ordDetDisTotal { get; set; }
        public Nullable<double> ordDetFinalTotal { get; set; }
        public Nullable<double> ordDetProDefaultPrice { get; set; }
        public string ordDetStoID { get; set; }
        public Nullable<System.DateTime> ordDetCreatedOn { get; set; }
        public string ordDetCreatedBy { get; set; }
        public Nullable<System.DateTime> ordDetModifiedOn { get; set; }
        public string ordDetModifiedBy { get; set; }
        public Nullable<int> ordDetStatus { get; set; }
        public Nullable<bool> ordDetIsDeleted { get; set; }
        public Nullable<bool> ordDetIsActive { get; set; }
        public Nullable<bool> ordPersonalDel { get; set; }
        public Nullable<double> ordDelCharges { get; set; }
        public Nullable<double> ordDelChargeOriginal { get; set; }
        public string ordDetDelNote { get; set; }
        public string ordDetWingCorTyp { get; set; }
        public string ordDeliveryType { get; set; }
    }

    public class OrderDiscountDetailEntity
    {
        public System.Guid ordDisDetID { get; set; }
        public string ordID { get; set; }
        public string stoOffID { get; set; }
        public Nullable<System.DateTime> ordDisDetCreatedOn { get; set; }
        public string ordDisDetCreatedBy { get; set; }
        public Nullable<System.DateTime> ordDisDetModifiedOn { get; set; }
        public string ordDisDetModifiedBy { get; set; }
        public Nullable<bool> ordDisDetIsDeleted { get; set; }
        public Nullable<bool> ordDisDetIsActive { get; set; }
    }

    public  class OrdersPaymentEntity
    {
        public System.Guid ordPayID { get; set; }
        public string ordID { get; set; }
        public string ordPayTransactionID { get; set; }
        public string ordPayTransactionNo { get; set; }
        public string ordPayTransactionStatus { get; set; }
        public Nullable<double> ordPayAmount { get; set; }
        public Nullable<System.DateTime> ordPayCreatedOn { get; set; }
        public string ordPayCreatedBy { get; set; }
        public Nullable<System.DateTime> ordPayModifiedOn { get; set; }
        public string ordPayModifiedBy { get; set; }
        public Nullable<bool> ordPayIsDeleted { get; set; }
        public Nullable<bool> ordPayIsActive { get; set; }
    }

    public  class OrderStatusMastEntity
    {
        public int ordStatusID { get; set; }
        public string ordStatusName { get; set; }
        public string ordStatusNameAr { get; set; }
        public Nullable<bool> ordStatusIsActive { get; set; }
    }

    public partial class InvoiceEntity
    {
        public System.Guid InvoiceID { get; set; }
        public int InvoiceNo { get; set; }
        public string invOrderID { get; set; }
        public string invStoID { get; set; }
        public Nullable<System.DateTime> invCreatedDate { get; set; }
        public string invCreatedBy { get; set; }
        public Nullable<System.DateTime> invModifiedDate { get; set; }
        public string invModifiedBy { get; set; }
        public Nullable<bool> invIsActive { get; set; }
        public Nullable<bool> invIsDeleted { get; set; }
    }

 
}
