﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
   public class SponsorEntity
    {
        public System.Guid spoID { get; set; }
        public string spoName { get; set; }
        public string spoDescription { get; set; }
        public string spoImg { get; set; }
        public string spoLink { get; set; }
        public string spoSeoName { get; set; }
        public Nullable<System.DateTime> spoCreatedOn { get; set; }
        public string spoCreatedBy { get; set; }
        public Nullable<System.DateTime> spoModifiedOn { get; set; }
        public string spoModifiedBy { get; set; }
        public Nullable<bool> spoIsDeleted { get; set; }
        public Nullable<bool> spoIsActive { get; set; }
    }
}
