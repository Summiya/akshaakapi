﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class MessagesMainEntity
    {
        public System.Guid mesID { get; set; }
        public string mesSubject { get; set; }
        public string mesDetail { get; set; }
        public string mesTo { get; set; }
        public string mesBy { get; set; }
        public string mesCreatedDate { get; set; }
        public string mesTypeID { get; set; }
        public Nullable<bool> mesIsActive { get; set; }
        public Nullable<bool> mesIsRead { get; set; }
        public Nullable<bool> mesIsReadMer { get; set; }
        public int mesOrderNo { get; set; }
    }

    public class MessageThreadEntity
    {
        public System.Guid mesThreadID { get; set; }
        public string mesID { get; set; }
        public string mesThreadDetail { get; set; }
        public string mesThreadBy { get; set; }
        public string mesThreadCreatedDate { get; set; }
        public int mesThreadOrderNo { get; set; }
    }
}
