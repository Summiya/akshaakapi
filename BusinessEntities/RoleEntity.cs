﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class RoleEntity
    {
        public System.Guid rolID { get; set; }
        public string rolName { get; set; }
        public Nullable<System.DateTime> rolCreatedOn { get; set; }
        public string rolCreatedBy { get; set; }
        public Nullable<System.DateTime> rolModifiedOn { get; set; }
        public string rolModifiedBy { get; set; }
        public Nullable<bool> rolIsDeleted { get; set; }
        public Nullable<bool> rolIsActive { get; set; }
    }

    public class RolesMappingEntity
    {
        public System.Guid rolMapID { get; set; }
        public string admRegID { get; set; }
        public string rolID { get; set; }
        public Nullable<System.DateTime> rolMapCreatedOn { get; set; }
        public string rolMapCreatedBy { get; set; }
        public Nullable<System.DateTime> rolMapModifiedOn { get; set; }
        public string rolMapModifiedBy { get; set; }
        public Nullable<bool> rolMapIsDeleted { get; set; }
        public Nullable<bool> rolMapIsActive { get; set; }
    }
}
