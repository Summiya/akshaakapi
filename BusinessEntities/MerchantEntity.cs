﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
   public class MerchantEntity
    {
        
            public System.Guid merID { get; set; }
            public string natID { get; set; }
            public string resID { get; set; }
            public string merFirstName { get; set; }
            public string merLastName { get; set; }
            public string merDesignation { get; set; }
            public string merDob { get; set; }
            public string merGender { get; set; }
            public string merLogTyp { get; set; }
            public string merLogTypID { get; set; }
            public string merImg { get; set; }
            public Nullable<bool> merIsVerified { get; set; }
            public Nullable<int> merStoreCount { get; set; }
            public Nullable<int> merCatCount { get; set; }
            public Nullable<int> merProCount { get; set; }
            public Nullable<bool> merPayment { get; set; }
            public string pacID { get; set; }
            public Nullable<System.DateTime> merPlanDate { get; set; }
            public Nullable<System.DateTime> merPlanRenewalDate { get; set; }
            public Nullable<System.DateTime> merCreatedOn { get; set; }
            public string merCreatedBy { get; set; }
            public Nullable<System.DateTime> merModifiedOn { get; set; }
            public string merModifiedBy { get; set; }
            public Nullable<bool> merIsDeleted { get; set; }
            public Nullable<bool> merIsActive { get; set; }

            public string Email { get; set; }
            public string Password { get; set; }

    }

    public class MerchantAddressEntity
    {
        public System.Guid merAddID { get; set; }
        public string merID { get; set; }
        public string merAddAddress { get; set; }
        public string merAddCity { get; set; }
        public string merAddState { get; set; }
        public string merAddCountry { get; set; }
        public string merAddZipcode { get; set; }
        public string merAddBuildingNo { get; set; }
        public string merAddAptNo { get; set; }
        public string merAddArea { get; set; }
        public string merAddLandMark { get; set; }
        public string merAddType { get; set; }
        public Nullable<System.DateTime> merAddCreatedOn { get; set; }
        public string merAddCreatedBy { get; set; }
        public Nullable<System.DateTime> merAddModifiedOn { get; set; }
        public string merAddModifiedBy { get; set; }
        public Nullable<bool> merAddIsDeleted { get; set; }
        public Nullable<bool> merAddIsActive { get; set; }
    }

    public  class MerchantBankDetailEntity
    {
        public System.Guid merBanDetID { get; set; }
        public string merID { get; set; }
        public string merBanDetAccountName { get; set; }
        public string merBanDetBankName { get; set; }
        public string merBanDetBranchName { get; set; }
        public string merBanDetIBANNo { get; set; }
        public string merBanDetSWIFTCode { get; set; }
        public Nullable<System.DateTime> merBanDetCreatedOn { get; set; }
        public string merBanDetCreatedBy { get; set; }
        public Nullable<System.DateTime> merBanDetModifiedOn { get; set; }
        public string merBanDetModifiedBy { get; set; }
        public Nullable<bool> merBanDetIsDeleted { get; set; }
        public Nullable<bool> merBanDetIsActive { get; set; }
        public Nullable<bool> merBanAlternativePay { get; set; }
    }

    public  class MerchantCardInfoEntity
    {
        public System.Guid merCarInfID { get; set; }
        public string merID { get; set; }
        public string merCarInfCardNo { get; set; }
        public string merCarInfCardName { get; set; }
        public string merCarInfExpMonth { get; set; }
        public string merCarInfExpYear { get; set; }
        public Nullable<System.DateTime> merCarInfCreatedOn { get; set; }
        public string merCarInfCreatedBy { get; set; }
        public Nullable<System.DateTime> merCarInfModifiedOn { get; set; }
        public string merCarInfModifiedBy { get; set; }
        public Nullable<bool> merCarInfIsDeleted { get; set; }
        public Nullable<bool> merCarInfIsActive { get; set; }
    }




}
