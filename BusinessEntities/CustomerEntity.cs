﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class CustomerEntity
    {
        public System.Guid cusID { get; set; }
        public string natID { get; set; }
        public string resID { get; set; }
        public string cusFirstName { get; set; }
        public string cusLastName { get; set; }
        public string cusDOB { get; set; }
        public string cusGender { get; set; }
        public Nullable<System.DateTime> cusCreatedOn { get; set; }
        public Nullable<System.DateTime> cusModifiedOn { get; set; }
        public string cusModifiedBy { get; set; }
        public Nullable<bool> cusIsGuest { get; set; }
        public Nullable<bool> cusIsDeleted { get; set; }
        public Nullable<bool> cusIsActive { get; set; }
    }

    public class CustomerAddress
    {
        public System.Guid cusAddID { get; set; }
        public string cusID { get; set; }
        public string citID { get; set; }
        public string cusAddAddress { get; set; }
        public Nullable<int> cusAddBVType { get; set; }
        public string cusAddCity { get; set; }
        public string cusAddState { get; set; }
        public string cusAddCountry { get; set; }
        public string cusAddZipcode { get; set; }
        public string cusAddArea { get; set; }
        public string cusAddBuilding { get; set; }
        public string cusAddAptNo { get; set; }
        public string cusAddLongitude { get; set; }
        public string cusAddLatitude { get; set; }
        public string cusAddType { get; set; }
        public string cusAddStreet { get; set; }
        public string cusAddFloor { get; set; }
        public string cusAddFirstName { get; set; }
        public string cusAddLastName { get; set; }
        public string cusAddMobileNo { get; set; }
        public Nullable<System.DateTime> cusAddCreatedOn { get; set; }
        public string cusAddCreatedBy { get; set; }
        public Nullable<System.DateTime> cusAddModifiedOn { get; set; }
        public string cusAddModifiedBy { get; set; }
        public Nullable<bool> cusAddIsDeleted { get; set; }
        public Nullable<bool> cusAddIsActive { get; set; }
    }

    public class CustomerCardInfo
    {
        public System.Guid cusCarInfID { get; set; }
        public string cusID { get; set; }
        public string cusCarInfCardNo { get; set; }
        public string cusCarInfCardName { get; set; }
        public string cusCarInfExpMonth { get; set; }
        public string cusCarInfExpYear { get; set; }
        public Nullable<System.DateTime> cusCarInfCreatedOn { get; set; }
        public string cusCarInfCreatedBy { get; set; }
        public Nullable<System.DateTime> cusCarInfModifieOn { get; set; }
        public string cusCarInfModifiedBy { get; set; }
        public Nullable<bool> cusCarInfIsDeleted { get; set; }
        public Nullable<bool> cusCarInfIsActive { get; set; }
    }

    


}
