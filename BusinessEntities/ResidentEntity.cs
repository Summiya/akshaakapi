﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
   public class ResidentEntity
    {
        public System.Guid resID { get; set; }
        public string resName { get; set; }
        public string resNameAr { get; set; }
        public Nullable<System.DateTime> resCreatedOn { get; set; }
        public string resCreatedBy { get; set; }
        public Nullable<System.DateTime> resModifiedOn { get; set; }
        public string resModifiedBy { get; set; }
        public Nullable<bool> resIsDeleted { get; set; }
        public Nullable<bool> resIsActive { get; set; }
    }
}
