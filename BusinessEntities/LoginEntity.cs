﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{


    public class LoginEntity
    {
        public System.Guid cusLogID { get; set; }
        public string cusID { get; set; }
        public string cusLogEmail { get; set; }
        public string cusLogMobile { get; set; }
        public string cusLogType { get; set; }
        public Nullable<bool> cusLogIsOnline { get; set; }
        public Nullable<bool> cusLogIsVerified { get; set; }
        public string cusOTP { get; set; }
        public Nullable<System.DateTime> cusLogCreatedOn { get; set; }
        public Nullable<System.DateTime> cusLogModifiedOn { get; set; }
        public Nullable<bool> cusLogIsDeleted { get; set; }
        public Nullable<bool> cusLogIsActive { get; set; }

        public string Password { get; set; }
        public bool IsMobile { get; set; }

    }



    public class SuperAdminLoginEntity
    {
        public System.Guid supAdmLogID { get; set; }
        public string supAdmLogUserName { get; set; }
        public string supAdmLogPassword { get; set; }
        public Nullable<System.DateTime> supAdmLogCreatedOn { get; set; }
        public string supAdmLogCreatedBy { get; set; }
        public Nullable<System.DateTime> supAdmLogModifiedOn { get; set; }
        public string supAdmLogModifiedBy { get; set; }
        public Nullable<bool> supAdmLogIsDeleted { get; set; }
        public Nullable<bool> supAdmLogIsActive { get; set; }
    }


}
