﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
   
        public class InventoryProductEntity
        {
            public System.Guid invProID { get; set; }
            public string proID { get; set; }
            public Nullable<double> invProStockQty { get; set; }
            public Nullable<double> invProSalesQty { get; set; }
            public Nullable<double> invProReturnedQty { get; set; }
            public Nullable<double> invProDamagedQty { get; set; }
            public Nullable<double> invProToOSQty { get; set; }
            public Nullable<double> invProMinCartQty { get; set; }
            public Nullable<double> invProMaxCartQty { get; set; }
            public Nullable<double> invProMaxNotifQty { get; set; }
            public Nullable<System.DateTime> invProCreatedOn { get; set; }
            public string invProCreatedBy { get; set; }
            public Nullable<System.DateTime> invProModifiedOn { get; set; }
            public string invProModifiedBy { get; set; }
            public Nullable<bool> invProIsDeleted { get; set; }
            public Nullable<bool> invProIsActive { get; set; }
        }

        public class InventoryProductLogEntity
        {
            public System.Guid invProLogID { get; set; }
            public string proID { get; set; }
            public string invProLogType { get; set; }
            public string invVarID { get; set; }
            public Nullable<double> invProLogQty { get; set; }
            public Nullable<System.DateTime> invProLogDate { get; set; }
            public Nullable<System.DateTime> invProLogCreatedOn { get; set; }
            public string invProLogCreatedBy { get; set; }
            public Nullable<System.DateTime> invProLogModifiedOn { get; set; }
            public string invProLogModifiedBy { get; set; }
            public Nullable<bool> invProLogIsDeleted { get; set; }
            public Nullable<bool> invProLogIsActive { get; set; }
        }

        public class InventoryVariantEntity
        {
            public System.Guid invVarID { get; set; }
            public string proID { get; set; }
            public string proConID { get; set; }
            public Nullable<double> invVarQty { get; set; }
            public Nullable<double> invVarPerDayQty { get; set; }
            public Nullable<double> invVarPrice { get; set; }
            public Nullable<double> invVarDisValue { get; set; }
            public Nullable<double> invVarDisAmount { get; set; }
            public Nullable<double> invVarFinalAmount { get; set; }
            public Nullable<System.DateTime> invVarCreatedOn { get; set; }
            public string invVarCreatedBy { get; set; }
            public Nullable<System.DateTime> invVarModifiedOn { get; set; }
            public string invVarModifiedBy { get; set; }
            public Nullable<bool> invVarIsDeleted { get; set; }
            public Nullable<bool> invVarIsActive { get; set; }
        }

}
