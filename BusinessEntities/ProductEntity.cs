﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class ProductEntity
    {
        public System.Guid proID { get; set; }
        public string stoID { get; set; }
        public string catID { get; set; }
        public string stoTaxID { get; set; }
        public string stoDisID { get; set; }
        public string proCode { get; set; }
        public string proType { get; set; }
        public string proName { get; set; }
        public string proNameArb { get; set; }
        public string proSeoName { get; set; }
        public string proDescription { get; set; }
        public string proDescriptionArb { get; set; }
        public string proImg { get; set; }
        public Nullable<double> proDefaultPrice { get; set; }
        public Nullable<double> proAvailableQty { get; set; }
        public Nullable<double> proPerDayQty { get; set; }
        public string prodTimID { get; set; }
        public string proMetaKeyword { get; set; }
        public string proMetaKeywordArb { get; set; }
        public string proMetaDescription { get; set; }
        public string proMetaDescriptionArb { get; set; }
        public Nullable<bool> proHideFromShop { get; set; }
        public string excRetTimID { get; set; }
        public Nullable<bool> proBuyResShipCost { get; set; }
        public Nullable<bool> proIamResShipCost { get; set; }
        public Nullable<double> proDisValue { get; set; }
        public Nullable<double> proDisAmount { get; set; }
        public Nullable<double> proFinalAmount { get; set; }
        public Nullable<bool> proIsRecomemded { get; set; }
        public Nullable<int> proWeight { get; set; }
        public Nullable<bool> proIsPopular { get; set; }
        public Nullable<bool> proIsSubmitted { get; set; }
        public Nullable<bool> proIsApproved { get; set; }
        public Nullable<System.DateTime> proCreatedOn { get; set; }
        public string proCreatedBy { get; set; }
        public Nullable<System.DateTime> proModifiedOn { get; set; }
        public string proModifiedBy { get; set; }
        public Nullable<bool> proIsDeleted { get; set; }
        public Nullable<bool> proIsActive { get; set; }
    }

    public class ProductBadgesEntity
    {
        public int Id { get; set; }
        public string StoId { get; set; }
        public string ProductId { get; set; }
        public string BadgeId { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> Priority { get; set; }
    }

    public class ProductContainEntity
    {
        public System.Guid proConID { get; set; }
        public string catID { get; set; }
        public string proConName { get; set; }
        public string proConNameAr { get; set; }
        public string proConSeoName { get; set; }
        public string proConDescription { get; set; }
        public string proConIcon { get; set; }
        public Nullable<int> proSortNo { get; set; }
        public Nullable<System.DateTime> proConCreatedOn { get; set; }
        public string proConCreatedBy { get; set; }
        public Nullable<System.DateTime> proConModifiedOn { get; set; }
        public string proConModifedBy { get; set; }
        public Nullable<bool> proConIsDeleted { get; set; }
        public Nullable<bool> proConIsActive { get; set; }
    }

    public  class ProductContainsMappingEntity
    {
        public System.Guid proConMapID { get; set; }
        public string proID { get; set; }
        public string proConID { get; set; }
        public Nullable<System.DateTime> proConMapCreatedOn { get; set; }
        public string proConMapCreatedBy { get; set; }
    }

    public partial class ProductGallaryEntity
    {
        public System.Guid proGalID { get; set; }
        public string proID { get; set; }
        public string proGalImgName { get; set; }
        public Nullable<System.DateTime> proGalCreatedOn { get; set; }
        public string proGalCreatedBy { get; set; }
        public Nullable<System.DateTime> proGalModifiedOn { get; set; }
        public string proGalModifiedBy { get; set; }
        public Nullable<bool> proGalIsDeleted { get; set; }
        public Nullable<bool> proGalIsActive { get; set; }
    }

    public class ProductKeywordEntity
    {
        public System.Guid Id { get; set; }
        public string KeywordEn { get; set; }
    }

    public partial class ProductKeywordsArabicEntity
    {
        public System.Guid Id { get; set; }
        public string KeywordAr { get; set; }
    }


}
