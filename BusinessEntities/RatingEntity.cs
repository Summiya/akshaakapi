﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
   public class RatingEntity
    {
        public System.Guid ratID { get; set; }
        public string stoID { get; set; }
        public string cusID { get; set; }
        public string ordID { get; set; }
        public string ratRemarks { get; set; }
        public int ratValue { get; set; }
        public Nullable<System.DateTime> ratDate { get; set; }
        public Nullable<System.DateTime> ratCreatedOn { get; set; }
        public string ratCreatedBy { get; set; }
        public Nullable<System.DateTime> ratModifiedOn { get; set; }
        public string ratModifiedBy { get; set; }
        public Nullable<bool> ratIsDeleted { get; set; }
        public Nullable<bool> ratIsActive { get; set; }
    }

    public class RatingProductEntity
    {
        public System.Guid proRatID { get; set; }
        public string proID { get; set; }
        public string cusID { get; set; }
        public string ordID { get; set; }
        public string proRatRemarks { get; set; }
        public Nullable<int> proRatValue { get; set; }
        public Nullable<System.DateTime> proRatDate { get; set; }
        public Nullable<System.DateTime> proRatCreatedOn { get; set; }
        public string proRatCreatedBy { get; set; }
        public Nullable<System.DateTime> proRatModifiedOn { get; set; }
        public string proRatModifiedBy { get; set; }
        public Nullable<bool> proRatIsDeleted { get; set; }
        public Nullable<bool> proRatIsActive { get; set; }
    }
}
