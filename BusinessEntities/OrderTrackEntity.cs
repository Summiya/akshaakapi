﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{

    public class TrackOrderEntity
    {
        public System.Guid traOrdID { get; set; }
        public string ordDetID { get; set; }
        public Nullable<int> ordStaID { get; set; }
        public string traOrdText { get; set; }
        public Nullable<System.DateTime> traOrdDate { get; set; }
        public Nullable<System.DateTime> traOrdCreatedOn { get; set; }
        public string traOrdCreatedBy { get; set; }
        public Nullable<System.DateTime> traOrdModifiedOn { get; set; }
        public string traOrdModifiedBy { get; set; }
        public Nullable<bool> traOrdIsDeleted { get; set; }
        public Nullable<bool> traOrdIsActive { get; set; }
    }



    public class TrackingAramex
    {
        public System.Guid Id { get; set; }
        public string AramexID { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string OrderDetailID { get; set; }
        public string MerchantId { get; set; }
        public string OrderID { get; set; }
        public string FileURL { get; set; }
        public Nullable<bool> IsEmailSent { get; set; }
    }
}
