﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class EmailSettingEntity
    {
        public System.Guid settingId { get; set; }
        public string smtpHost { get; set; }
        public string smtpPort { get; set; }
        public string smtpEmail { get; set; }
        public string smtpPassword { get; set; }
        public Nullable<bool> smtpSSL { get; set; }
        public string smsUsername { get; set; }
        public string smsPassword { get; set; }
        public string smsLink { get; set; }
        public string smsApiKey { get; set; }
        public Nullable<System.DateTime> smtpCreatedOn { get; set; }
        public string smtpCreatedBy { get; set; }
        public Nullable<System.DateTime> smtpModifiedOn { get; set; }
        public string smtpModifiedBy { get; set; }
    }

    public class MailTemplateContentEntity
    {
        public string mailerCode { get; set; }
        public string subjectEN { get; set; }
        public string bodyEN { get; set; }
        public string smsEN { get; set; }
        public string subjectAR { get; set; }
        public string bodyAR { get; set; }
        public string smsAR { get; set; }
    }



}
