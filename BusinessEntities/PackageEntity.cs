﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class PackageEntity
    {
        public System.Guid pacID { get; set; }
        public string pacName { get; set; }
        public string pacDescription { get; set; }
        public Nullable<int> pacStoresCount { get; set; }
        public Nullable<int> pacCategoriesCount { get; set; }
        public Nullable<int> pacProductCount { get; set; }
        public Nullable<double> pacPrice { get; set; }
        public string pacType { get; set; }
        public string taxID { get; set; }
        public Nullable<double> pacTaxValue { get; set; }
        public Nullable<double> pacTaxAmount { get; set; }
        public string disID { get; set; }
        public Nullable<double> pacDisValue { get; set; }
        public Nullable<double> pacDisAmount { get; set; }
        public Nullable<double> pacFinalPrice { get; set; }
        public Nullable<System.DateTime> pacCreatedOn { get; set; }
        public string pacCreatedBy { get; set; }
        public Nullable<System.DateTime> pacModifiedOn { get; set; }
        public string pacModifiedBy { get; set; }
        public Nullable<bool> pacIsDeleted { get; set; }
        public Nullable<bool> pacIsActive { get; set; }
    }
}
