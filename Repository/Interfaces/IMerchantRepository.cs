﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GenericRepository;
using DataBase;

namespace Repository.Interfaces
{
   public interface IMerchantRepository : IRepository<Merchant>
    {
        List<Merchant> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        Merchant GetRecordByID(Guid Id);
    }
}
