﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GenericRepository;
using DataBase;
using Repository.Interfaces;
using System.Data.SqlClient;

namespace Repository
{
   public class MerchantRepository : Repository<Merchant>, IMerchantRepository
    {
        public MerchantRepository(AkShaakEntitiesDB context)
           : base(context)
        {

        }

        public List<Merchant> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null)
        {
            var parameters = new SqlParameter[5];
            if (string.IsNullOrEmpty(SearchValue))
            {
                parameters[0] = new SqlParameter { ParameterName = "@SearchValue", Value = DBNull.Value };
            }
            else
            {
                parameters[0] = new SqlParameter { ParameterName = "@SearchValue", Value = SearchValue };

            }

            parameters[1] = new SqlParameter { ParameterName = "@PageNo", Value = PageNo };
            parameters[2] = new SqlParameter { ParameterName = "@PageSize", Value = PageSize };
            if (string.IsNullOrEmpty(SearchValue))
            {
                parameters[3] = new SqlParameter { ParameterName = "@SortColumn", Value = DBNull.Value };
            }
            else
            {
                parameters[3] = new SqlParameter { ParameterName = "@SortColumn", Value = SortColumn };

            }
            if (string.IsNullOrEmpty(SearchValue))
            {
                parameters[4] = new SqlParameter { ParameterName = "@SortOrder", Value = DBNull.Value };
            }
            else
            {
                parameters[4] = new SqlParameter { ParameterName = "@SortOrder", Value = SortOrder };


            }

            var MerchantData = SQLQuery<Merchant>(
                 "exec usp_GetAllMerchants @SearchValue, @PageNo, @PageSize, @SortColumn, @SortOrder ", parameters).ToList();

            if (MerchantData.Any())
            {
              return MerchantData;
            }
            return null;
        }

        public Merchant GetRecordByID(Guid Id)
        {
            var MerchantData = Context.Merchants.Where(x => x.merID == Id).FirstOrDefault();

            return MerchantData;
        }
    }
}
