﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IPaymentType
    {
        /* Methods */
        List<PaymentTypeEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        PaymentTypeEntity GetRecordByID(Guid Id);
        Guid Save(PaymentTypeEntity _Entity);
        PaymentTypeEntity Update(PaymentTypeEntity _Entity);
        void Delete(Guid Id);
    }
}
