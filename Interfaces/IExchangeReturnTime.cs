﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
   public interface IExchangeReturnTimeEntity
    {
        /* Methods */
        List<ExchangeReturnTimeEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ExchangeReturnTimeEntity GetRecordByID(Guid Id);
        Guid Save(ExchangeReturnTimeEntity _Entity);
        ExchangeReturnTimeEntity Update(ExchangeReturnTimeEntity _Entity);
        void Delete(Guid Id);
    }
}
