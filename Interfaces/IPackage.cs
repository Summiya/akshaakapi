﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IPackage
    {
        /* Methods */
        List<PackageEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        PackageEntity GetRecordByID(Guid Id);
        Guid Save(PackageEntity _Entity);
        PackageEntity Update(PackageEntity _Entity);
        void Delete(Guid Id);
    }
}
