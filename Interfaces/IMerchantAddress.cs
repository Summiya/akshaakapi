﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IMerchantAddress
    {
        /*  Methods */
        List<MerchantAddressEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        MerchantAddressEntity GetRecordByID(Guid Id);
        Guid Save(MerchantAddressEntity _Entity);
        MerchantAddressEntity Update(MerchantAddressEntity _Entity);
        void Delete(Guid Id);
    }
}
