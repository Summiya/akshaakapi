﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
   public interface ICartWhishlist
    {
        /* CartWhishlist Methods */
        List<CartWhishlistEntity> GetAllCartWishList(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        List<CartWhishlistEntity> GetAllCartWishListByCustomer(Guid customerID);

        CartWhishlistEntity GetCartWishlist(Guid Id);
        Guid Save(CartWhishlistEntity _Entity);
        CartWhishlistEntity Update(CartWhishlistEntity _Entity);
        void Delete(Guid Id);
    }
}
