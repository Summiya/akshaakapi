﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IRating
    {
        /* Badge Methods */
        List<RatingEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        RatingEntity GetRecordByID(Guid Id);
        Guid Save(RatingEntity _Entity);
        RatingEntity Update(RatingEntity _Entity);
        void Delete(Guid Id);
    }
}
