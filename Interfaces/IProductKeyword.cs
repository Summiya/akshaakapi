﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IProductKeyword
    {
        /*  Methods */
        List<ProductKeywordEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ProductKeywordEntity GetRecordByID(Guid Id);
        Guid Save(ProductKeywordEntity _Entity);
        ProductKeywordEntity Update(ProductKeywordEntity _Entity);
        void Delete(Guid Id);
    }
}
