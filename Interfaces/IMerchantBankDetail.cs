﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    public interface IMerchantBankDetail
    {
        /*  Methods */       
        void Save(MerchantBankDetailEntity _Entity);       
        
    }
}
