﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
   public interface IDiscount
    {
        /* Methods */
        List<DiscountEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        DiscountEntity GetRecordByID(Guid Id);
        Guid Save(DiscountEntity _Entity);
        DiscountEntity Update(DiscountEntity _Entity);
        void Delete(Guid Id);

    }
}
