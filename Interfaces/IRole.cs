﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IRole
    {
        /*  Methods */
        List<RoleEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        RoleEntity GetRecordByID(Guid Id);
        Guid Save(RoleEntity _Entity);
        RoleEntity Update(RoleEntity _Entity);
        void Delete(Guid Id);
    }
}
