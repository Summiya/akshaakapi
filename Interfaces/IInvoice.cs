﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IInvoice
    {
        /* Badge Methods */
        List<InvoiceEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        InvoiceEntity GetRecordByID(Guid Id);
        Guid Save(InvoiceEntity _Entity);
        InvoiceEntity Update(InvoiceEntity _Entity);
        void Delete(Guid Id);
    }
}
