﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
   public interface IDeliveryDetail
    {
        /* DeliveryDetail Methods */
        List<DeliveryDetailEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        DeliveryDetailEntity GetRecordByID(Guid Id);
        Guid Save(DeliveryDetailEntity _Entity);
        DeliveryDetailEntity Update(DeliveryDetailEntity _Entity);
        void Delete(Guid Id);
    }
}
