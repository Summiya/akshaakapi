﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IShopCategory
    {
        /*  Methods */
        List<ShopCategoryEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ShopCategoryEntity GetRecordByID(Guid Id);
        Guid Save(ShopCategoryEntity _Entity);
        ShopCategoryEntity Update(ShopCategoryEntity _Entity);
        void Delete(Guid Id);
    }

}
