﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    public interface IContactUs
    {
        /* ContactUs Methods */
        List<ContactUsEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ContactUsEntity GetRecordByID(Guid Id);
        Guid Save(ContactUsEntity _Entity);
        ContactUsEntity Update(ContactUsEntity _Entity);
        void Delete(Guid Id);
    }
}
