﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
   public interface IStoreTiming
    {
        void Save( StoreTimingEntity strTiming);
        void Delete(Guid Id);
    }
}
