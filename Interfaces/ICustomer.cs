﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    public interface ICustomer
    {
         bool CheckEmailIfExist(string cusLogEmail);

        /* Customer Methods */
        List<CustomerEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        CustomerEntity GetRecordByID(Guid Id);
        Guid Save(CustomerEntity _Entity);
        AttributeEntity Update(CustomerEntity _Entity);
        void Delete(Guid Id);
    }
}
