﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IProductKeywordsArabic
    {
        /*  Methods */
        List<ProductKeywordsArabicEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ProductKeywordsArabicEntity GetRecordByID(Guid Id);
        Guid Save(ProductKeywordsArabicEntity _Entity);
        ProductKeywordsArabicEntity Update(ProductKeywordsArabicEntity _Entity);
        void Delete(Guid Id);
    }
}
