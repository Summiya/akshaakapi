﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IStoreOffer
    {
        /*  Methods */
        List<StoreOffreEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        StoreOffreEntity GetRecordByID(Guid Id);
        Guid Save(StoreOffreEntity _Entity);
        StoreOffreEntity Update(StoreOffreEntity _Entity);
        void Delete(Guid Id);
    }
}
