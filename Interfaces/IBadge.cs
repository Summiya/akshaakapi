﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace BusinessEntities
{
   public interface IBadge
    {
     
        /* Badge Methods */
        List<BadgeEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        BadgeEntity GetRecordByID(Guid Id);
        Guid Save(BadgeEntity _Entity);
        BadgeEntity Update(BadgeEntity _Entity);
        void Delete(Guid Id);

    }
}
