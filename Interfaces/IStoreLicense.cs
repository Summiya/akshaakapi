﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IStoreLicense
    {
        /* Methods */
        List<StoreLicenseEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        StoreLicenseEntity GetRecordByID(Guid Id);
        Guid Save(StoreLicenseEntity _Entity);
        StoreLicenseEntity Update(StoreLicenseEntity _Entity);
        void Delete(Guid Id);
    }
}
