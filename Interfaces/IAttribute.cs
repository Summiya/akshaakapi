﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    public interface IAttribute
    {
        /* Attribute Methods */ 
        List<AttributeEntity> GetAllAttributes(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        AttributeEntity GetAttributebyID(Guid Id);
        Guid SaveAttribute(AttributeEntity _attributeEntity);
        AttributeEntity UpdateAttribute(AttributeEntity _attributeEntity);
        void DeleteAttributebyId(Guid Id);

        /* AttributesOption Methods */
        List<AttributesOptionEntity> GetAllAttributesOptions(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        AttributesOptionEntity GetAttributeOptionbyID(Guid Id);
        Guid SaveAttributeOptions(AttributesOptionEntity _attributeEntity);
        AttributesOptionEntity UpdateAttributeOptionste(AttributesOptionEntity _attributeEntity);
        void DeleteAttributeOptionbyId(Guid Id);
    }
}
