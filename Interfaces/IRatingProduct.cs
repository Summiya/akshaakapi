﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IRatingProduct
    {
        /* Badge Methods */
        List<RatingProductEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        RatingProductEntity GetRecordByID(Guid Id);
        Guid Save(RatingProductEntity _Entity);
        RatingProductEntity Update(RatingProductEntity _Entity);
        void Delete(Guid Id);
    }
}
