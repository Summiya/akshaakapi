﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IOrderDiscountDetail
    {
        /* Badge Methods */
        List<OrderDiscountDetailEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        OrderDiscountDetailEntity GetRecordByID(Guid Id);
        Guid Save(OrderDiscountDetailEntity _Entity);
        OrderDiscountDetailEntity Update(OrderDiscountDetailEntity _Entity);
        void Delete(Guid Id);
    }
}
