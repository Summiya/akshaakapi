﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    public interface IMailTemplateContent
    {
        /* Badge Methods */
        List<MailTemplateContentEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        MailTemplateContentEntity GetRecordByID(Guid Id);
        Guid Save(MailTemplateContentEntity _Entity);
        MailTemplateContentEntity Update(MailTemplateContentEntity _Entity);
        void Delete(Guid Id);
    }
}
