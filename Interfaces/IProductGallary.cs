﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IProductGallary
    {
        /* Badge Methods */
        List<ProductGallaryEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ProductGallaryEntity GetRecordByID(Guid Id);
        Guid Save(ProductGallaryEntity _Entity);
        ProductGallaryEntity Update(ProductGallaryEntity _Entity);
        void Delete(Guid Id);
    }
}
