﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IStoreDeliveryMapping
    {
        /*  Methods */
        List<StoreDeliveryMappingEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        StoreDeliveryMappingEntity GetRecordByID(Guid Id);
        Guid Save(StoreDeliveryMappingEntity _Entity);
        StoreDeliveryMappingEntity Update(StoreDeliveryMappingEntity _Entity);
        void Delete(Guid Id);
    }
}
