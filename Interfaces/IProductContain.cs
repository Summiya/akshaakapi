﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IProductContain
    {
        /*  Methods */
        List<ProductContainEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ProductContainEntity GetRecordByID(Guid Id);
        Guid Save(ProductContainEntity _Entity);
        ProductContainEntity Update(ProductContainEntity _Entity);
        void Delete(Guid Id);
    }
}
