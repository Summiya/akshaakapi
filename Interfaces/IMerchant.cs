﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
   public interface IMerchant 

    {
        /*  Methods */
        List<MerchantEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        MerchantEntity GetRecordByID(Guid Id);
        Guid Save(MerchantEntity _Entity);
        MerchantEntity Update(MerchantEntity _Entity);
        void Delete(Guid Id);

        bool ISMerchantExist(string email);
    }
}
