﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IProductContainsMapping
    {
        /*  Methods */
        List<ProductContainsMappingEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ProductContainsMappingEntity GetRecordByID(Guid Id);
        Guid Save(ProductContainsMappingEntity _Entity);
        ProductContainsMappingEntity Update(ProductContainsMappingEntity _Entity);
        void Delete(Guid Id);
    }
}
