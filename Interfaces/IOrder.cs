﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IOrder
    {
        /* Badge Methods */
        List<OrderEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        OrderEntity GetRecordByID(Guid Id);
        Guid Save(OrderEntity _Entity);
        OrderEntity Update(OrderEntity _Entity);
        void Delete(Guid Id);
    }
}
