﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
   public interface IMessagesMain
    {
        /*  Methods */
        List<MessagesMainEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        MessagesMainEntity GetRecordByID(Guid Id);
        Guid Save(MessagesMainEntity _Entity);
        MessagesMainEntity Update(MessagesMainEntity _Entity);
        void Delete(Guid Id);
    }
}
