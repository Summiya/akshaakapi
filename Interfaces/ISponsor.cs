﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface ISponsor
    {
        /*  Methods */
        List<SponsorEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        SponsorEntity GetRecordByID(Guid Id);
        Guid Save(SponsorEntity _Entity);
        SponsorEntity Update(SponsorEntity _Entity);
        void Delete(Guid Id);
    }
}
