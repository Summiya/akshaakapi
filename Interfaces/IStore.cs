﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    public interface IStore
    {
        Guid Save(StoreEntity storeEntity);
        
        StoreEntity GetStorebyMerchantID(Guid merchantID);
        StoreEntity GetStorebyStoreID(Guid Id);
    }
}
