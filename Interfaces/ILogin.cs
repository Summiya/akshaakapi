﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BusinessEntities;

namespace Interfaces
{
    public interface ILogin 
    {
        /* Badge Methods */
        List<LoginEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        LoginEntity GetRecordByID(Guid Id);
        Guid Save(LoginEntity _Entity);
        LoginEntity Update(LoginEntity _Entity);
        void Delete(Guid Id);
    }
}
