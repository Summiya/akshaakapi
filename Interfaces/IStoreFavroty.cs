﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IStoreFavroty
    {
        /*  Methods */
        List<StoreFavrotyEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        StoreFavrotyEntity GetRecordByID(Guid Id);
        Guid Save(StoreFavrotyEntity _Entity);
        StoreFavrotyEntity Update(StoreFavrotyEntity _Entity);
        void Delete(Guid Id);
    }
}
