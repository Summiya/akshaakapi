﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
   public  interface IExchangeReturnPol
    {
        /*  Methods */
        List<ExchangeReturnPolEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ExchangeReturnPolEntity GetRecordByID(Guid Id);
        Guid Save(ExchangeReturnPolEntity _Entity);
        AttributeEntity Update(ExchangeReturnPolEntity _Entity);
        void Delete(Guid Id);
    }
}
