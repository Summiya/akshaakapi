﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IMerchantCardInfo
    {
        /* Methods */
        List<MerchantCardInfoEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        MerchantCardInfoEntity GetRecordByID(Guid Id);
        Guid Save(MerchantCardInfoEntity _Entity);
        MerchantCardInfoEntity Update(MerchantCardInfoEntity _Entity);
        void Delete(Guid Id);
    }
}
