﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IPaymentsInfo
    {
        /* Methods */
        List<PaymentsInfoEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        PaymentsInfoEntity GetRecordByID(Guid Id);
        Guid Save(PaymentsInfoEntity _Entity);
        PaymentsInfoEntity Update(PaymentsInfoEntity _Entity);
        void Delete(Guid Id);
    }
}
