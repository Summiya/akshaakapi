﻿using BusinessEntities;
using System;
using System.Collections.Generic;


namespace Interfaces
{
   public interface ICommon
    {
        List<CommonEntity> GetAllCountries();
        List<CityEntity> GetAllCities();
        List<CityAreaEntity> GetAllAreasByCity(Guid CityID);

        List<MenuEntity> GetAllMenus();
        List<MenuMappingEntity> GetAllMenuMapping(Guid _menuID, Guid rollID);
        
        List<NationalityEntity> GetAllNationality();

        List<NGOMasterEntity> GetAllNGOMaster();
       
        List<OfferTypeEntity> GetAllOfferType();

        List<SocialMediaMasterEntity> GetAllSocialMediaMaster();

        List<WeekDayEntity> GetAllSocialWeekDay();
    }
}
