﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IProductionTime
    {
        /* Badge Methods */
        List<ProductionTimeEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ProductionTimeEntity GetRecordByID(Guid Id);
        Guid Save(ProductionTimeEntity _Entity);
        ProductionTimeEntity Update(ProductionTimeEntity _Entity);
        void Delete(Guid Id);
    }
}
