﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
   public interface IEmailSetting
    {
        /*  Methods */
        List<EmailSettingEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        EmailSettingEntity GetRecordByID(Guid Id);
        Guid Save(EmailSettingEntity _Entity);
        EmailSettingEntity Update(EmailSettingEntity _Entity);
        void Delete(Guid Id);

    }
}
