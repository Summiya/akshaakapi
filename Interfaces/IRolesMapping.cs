﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IRolesMapping
    {
        /*  Methods */
        List<RolesMappingEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        RolesMappingEntity GetRecordByID(Guid Id);
        Guid Save(RolesMappingEntity _Entity);
        RolesMappingEntity Update(RolesMappingEntity _Entity);
        void Delete(Guid Id);
    }
}
