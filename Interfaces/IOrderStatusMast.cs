﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IOrderStatusMast
    {
        /*  Methods */
        List<OrderStatusMastEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        OrderStatusMastEntity GetRecordByID(Guid Id);
        Guid Save(OrderStatusMastEntity _Entity);
        OrderStatusMastEntity Update(OrderStatusMastEntity _Entity);
        void Delete(Guid Id);
    }
}
