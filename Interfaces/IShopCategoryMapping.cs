﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IShopCategoryMapping
    {
        /*  Methods */
        List<ShopCategoryMappingEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ShopCategoryMappingEntity GetRecordByID(Guid Id);
        Guid Save(ShopCategoryMappingEntity _Entity);
        ShopCategoryMappingEntity Update(ShopCategoryMappingEntity _Entity);
        void Delete(Guid Id);
    }
}
