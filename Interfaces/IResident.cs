﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IResident
    {
        /*  Methods */
        List<ResidentEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ResidentEntity GetRecordByID(Guid Id);
        Guid Save(ResidentEntity _Entity);
        ResidentEntity Update(ResidentEntity _Entity);
        void Delete(Guid Id);
    }
}
