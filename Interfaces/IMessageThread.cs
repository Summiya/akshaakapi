﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IMessageThread
    {
         /*  Methods */
        List<MessageThreadEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        MessageThreadEntity GetRecordByID(Guid Id);
        Guid Save(MessageThreadEntity _Entity);
        MessageThreadEntity Update(MessageThreadEntity _Entity);
        void Delete(Guid Id);
    }
}
