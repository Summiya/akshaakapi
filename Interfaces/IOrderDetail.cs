﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace BusinessEntities
{
   public interface IOrderDetail
    {
     
        /* Methods */
        List<OrderDetailEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        OrderDetailEntity GetRecordByID(Guid Id);
        Guid Save(OrderDetailEntity _Entity);
        OrderDetailEntity Update(OrderDetailEntity _Entity);
        void Delete(Guid Id);

    }
}
