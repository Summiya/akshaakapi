﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface ICategoryMapping
    {
        /*  Methods */
        List<CategoryMappingEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        CategoryMappingEntity GetRecordByID(Guid Id);
        Guid Save(CategoryMappingEntity _Entity);
        CategoryMappingEntity Update(CategoryMappingEntity _Entity);
        void Delete(Guid Id);
    }
}
