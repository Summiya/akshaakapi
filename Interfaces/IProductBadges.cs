﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IProductBadges
    {
        /*  Methods */
        List<ProductBadgesEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ProductBadgesEntity GetRecordByID(Guid Id);
        Guid Save(ProductBadgesEntity _Entity);
        ProductBadgesEntity Update(ProductBadgesEntity _Entity);
        void Delete(Guid Id);
    }
}
