﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface ITax
    {
        /*  Methods */
        List<TaxEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        TaxEntity GetRecordByID(Guid Id);
        Guid Save(TaxEntity _Entity);
        TaxEntity Update(TaxEntity _Entity);
        void Delete(Guid Id);
    }
}
