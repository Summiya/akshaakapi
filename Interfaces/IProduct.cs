﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IProduct
    {
        /*  Methods */
        List<ProductEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        ProductEntity GetRecordByID(Guid Id);
        Guid Save(ProductEntity _Entity);
        ProductEntity Update(ProductEntity _Entity);
        void Delete(Guid Id);
    }
}
