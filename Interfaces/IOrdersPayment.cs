﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface IOrdersPayment
    {
        /* Methods */
        List<OrdersPaymentEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        OrdersPaymentEntity GetRecordByID(Guid Id);
        Guid Save(OrdersPaymentEntity _Entity);
        OrdersPaymentEntity Update(OrdersPaymentEntity _Entity);
        void Delete(Guid Id);
    }
}
