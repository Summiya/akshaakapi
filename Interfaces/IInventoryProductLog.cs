﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
   public interface IInventoryProductLog
    {
        /* Badge Methods */
        List<InventoryProductLogEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        InventoryProductLogEntity GetRecordByID(Guid Id);
        Guid Save(InventoryProductLogEntity _Entity);
        InventoryProductLogEntity Update(InventoryProductLogEntity _Entity);
        void Delete(Guid Id);
    }
}
