﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
    interface ISocialMedia
    {
        /* Methods */
        List<SocialMediaEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        SocialMediaEntity GetRecordByID(Guid Id);
        Guid Save(SocialMediaEntity _Entity);
        SocialMediaEntity Update(SocialMediaEntity _Entity);
        void Delete(Guid Id);
    }
}
