﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace Interfaces
{
  public  interface IInventoryProduct
    {
        /* Methods */
        List<InventoryProductEntity> GetAllRecords(string SearchValue, int PageNo, int PageSize, string SortColumn = null, string SortOrder = null);
        InventoryProductEntity GetRecordByID(Guid Id);
        Guid Save(InventoryProductEntity _Entity);
        InventoryProductEntity Update(InventoryProductEntity _Entity);
        void Delete(Guid Id);
    }
}
