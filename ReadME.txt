WEB API Resolver 

Add system.web.http in library using this command
Install-Package Microsoft.AspNet.WebApi.Core


System.Web.Http.WebHost.dll is required for global configurations



For API response Wrapper

ASP.NET Web API Integration
For standard ASP.NET Web API applications, you can do:


PM> Install-Package VMD.RESTApiResponseWrapper.Net -Version 1.0.3


After the installation, you can start integrating the wrapper to your ASP.NET Web API project by following the steps below:

Declare the following namespaces within WebApiConfig.cs:
Hide   Copy Code
using VMD.RESTApiResponseWrapper.Net;
using VMD.RESTApiResponseWrapper.Net.Filters;
Register the following within WebApiConfig.cs:
Hide   Copy Code
config.Filters.Add(new ApiExceptionFilter());
config.MessageHandlers.Add(new WrappingHandler());     
Done.

Note: The latest versions of both packages as of this time of writing is v1.0.3

Sample Response Output
The following are examples of response output:

Successful response format with data:

{
    "Version": "1.0.0.0",
    "StatusCode": 200,
    "Message": "Request successful.",
    "Result": [
        "value1",
        "value2"
    ]
}  
Successful response format without data:


{
    "Version": "1.0.0.0",
    "StatusCode": 201,
    "Message": "Student with ID 6 has been created."
}
Response format for validation errors:


{
    "Version": "1.0.0.0",
    "StatusCode": 400,
    "Message": "Request responded with exceptions.",
    "ResponseException": {
        "IsError": true,
        "ExceptionMessage": "Validation Field Error.",
        "Details": null,
        "ReferenceErrorCode": null,
        "ReferenceDocumentLink": null,
        "ValidationErrors": [
            {
                "Field": "LastName",
                "Message": "'Last Name' should not be empty."
            },
            {
                "Field": "FirstName",
                "Message": "'First Name' should not be empty."
            }
    ]
    }
}
Response format for errors:

{
    "Version": "1.0.0.0",
    "StatusCode": 404,
    "Message": "Unable to process the request.",
    "ResponseException": {
        "IsError": true,
        "ExceptionMessage": "The specified URI does not exist. Please verify and try again.",
                  "Details": null,
        "ReferenceErrorCode": null,
        "ReferenceDocumentLink": null,
        "ValidationErrors": null
    }
} 
Defining a Custom Exception
This library isn't just a middleware or a wrapper; it also provides a method that you can use for defining your own exception. For example, if you want to throw your own exception message, you could simply do:


throw new ApiException("Your Message",401, ModelStateExtension.AllErrors(ModelState));
The ApiException has the following parameters that you can set:


ApiException(string message,
             int statusCode = 500,
             IEnumerable<ValidationError> errors = null,
             string errorCode = "",
             string refLink = "")

Defining Your Own Response Object
Aside from throwing your own custom exception, you could also return your own custom defined JSON response by using the ApiResponse object in your API controller. For example:


return new APIResponse(201,"Created");
The APIResponse has the following parameters that you can set:


APIResponse(int statusCode,
            string message = "",
            object result = null,
            ApiError apiError = null,
            string apiVersion = "1.0.0.0")