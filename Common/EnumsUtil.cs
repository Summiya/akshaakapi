﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    class EnumsUtil
    {
        enum weekdays : int
        {
            Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
        }
    }


    public enum StatusType : int
    {
        Success = 200,
        Empty = 201,
        Error = 203,
        Duplicate = 204,
        InvalidCredentials = 205,
        NoActiveEmailExists = 206,
        EmailSent = 207,
        AccountBlocked = 208,
        InvalidUserName = 209,
        InvalidPassworde = 210,
        UserNameAlreadyExists = 211,
        UserEmailAlreadyExists = 212,
        UserEIDAlreadyExists = 213,
        UserMobileAlreadyExists = 214,
        Failure = 215,
        AccountIsInactive = 216
    }

    public class Constants
    {
        public const string Success_Message = "Success";
        public const string User_Already_Exist = "User already exist";
        public const string InvalidUser = "Invalid User Name/Password";
        public const string User_Updated_Successfully = "User updated successfully";
        public const string Password_Updated_Successfully = "Password updated successfully";
        public const string Password_Not_Correct = "Password not matches with current password";
        public const string No_Record_found = "No Record Found with ID ";
        public const string General_Error = "An Error has been Occured please try again";
    }
}
