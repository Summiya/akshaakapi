﻿using System.Net;
using System.IO;
using System.Web;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Web.Configuration;
using System.Net.Http;
using System.Web.Http;
using BusinessEntities;
using Interfaces;
using VMD.RESTApiResponseWrapper.Net.Wrappers;
using AkshaakWebAPI.Helpers;
using System.Net.Http.Formatting;
using System.Diagnostics;
using System.Text;
using Uploadfiles;
using System;

namespace AkshaakWebAPI.Controllers
{
    public class CommonController : ApiController
    {
        private readonly ICommon _common;


        public CommonController(ICommon commonservice)
        {
            _common = commonservice;
        }



        [HttpGet]
        [Route("api/Country/GetAllCountries/")]
        public APIResponse GetAllCountries()
        {
            var response = _common.GetAllCountries();
            return new APIResponse((int)HttpStatusCode.OK, "Success", response);
        }


        [HttpGet]
        [Route("api/Country/GetAllCities/")]
        public APIResponse GetAllCities()
        {
            var response = _common.GetAllCities();
            return new APIResponse((int)HttpStatusCode.OK, "Success", response);
        }


        [HttpGet]
        [Route("api/Country/GetAllAreaByCityID/cityID")]
        public APIResponse GetAllAreaByCityID(Guid cityID)
        {
            var response = _common.GetAllAreasByCity(cityID);
            return new APIResponse((int)HttpStatusCode.OK, "Success", response);
        }



    }
}
