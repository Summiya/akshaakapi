﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Web;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Web.Configuration;
using System.Net.Http;
using System.Web.Http;
using BusinessEntities;
using Interfaces;
using VMD.RESTApiResponseWrapper.Net.Wrappers;
using AkshaakWebAPI.Helpers;
using System.Net.Http.Formatting;
using System.Diagnostics;
using System.Text;
using Uploadfiles;

namespace AkshaakWebAPI.Controllers
{

    public class CategoryController : ApiController
    {

        private readonly ICategory _categoryservice;
        CategoryEntity _formcategoryentity = new CategoryEntity();
        CategoryEntity _categoryentity = null;
        string filepath = "Uploads/Categories";

        public CategoryController(ICategory categoryservice)
        {
            _categoryservice = categoryservice;
        }

        [HttpGet]
        [Route("api/GetAllCategories/")]
        public APIResponse GetAllCategories(string SearchValue = null, int PageNo = 1, int PageSize = 10, string SortColumn = null, string SortOrder = null)
        {
            var response = _categoryservice.GetAllCategories(SearchValue, PageNo, PageSize, SortColumn, SortOrder);
            return new APIResponse((int)HttpStatusCode.OK, "Success", response);
        }

        [HttpGet]
        [Route("api/GetCategorDetailbyId/{Id}")]
        public APIResponse GetCategorDetailbyId(string Id)
        {
            try
            {
                _categoryentity = _categoryservice.GetCategorDetailbyId(Guid.Parse(Id));

                if (_categoryentity != null)
                {

                    return new APIResponse((int)HttpStatusCode.OK, "Success", _categoryentity);
                }

                return new APIResponse((int)HttpStatusCode.NotFound, "No Record Found with ID " + Id);
            }
            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }
        }


        [HttpPost]
        [Route("api/AddCategory")]
        public async Task<APIResponse> AddCategory()
        {
           
            try
            {

                // Check if the request contains multipart/form-data.  
                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());
                //access form data  
                NameValueCollection formData = provider.FormData;

                //access files  
                IList<HttpContent> files = provider.Files;

                HttpContent file = files[0];
                
                string thisFileName = await Upload.Fileuploading(file, filepath);

                //example values:
                _formcategoryentity.catName = formData.GetValues("catName").First().ToString();
                _formcategoryentity.catDescription = formData.GetValues("catDescription").First();
                _formcategoryentity.catImg = thisFileName;
                
                _categoryservice.SaveData(_formcategoryentity);
                
                return new APIResponse((int)HttpStatusCode.OK, "Success", _formcategoryentity);

            }
            catch (Exception ex)
            {
                return new APIResponse((int)HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        [HttpPost]
        [Route("api/UpdateCategory")]
        public async Task<APIResponse> UpdateCategory()
        {
            try
            {
                
                var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());
                //access form data  
                NameValueCollection formData = provider.FormData;

                //access files  
                IList<HttpContent> files = provider.Files;

                Guid Id = new Guid(formData.GetValues("catID").First());

                _categoryentity = _categoryservice.GetCategorDetailbyId(Id);

                if (_categoryentity != null)
                {
                    
                if (files.Count > 0 )
                {
                    HttpContent file = files[0];
                    string thisFileName = await Upload.Fileuploading(file, filepath);
                    _categoryentity.catImg = thisFileName;
                }

                
                
                //example values:
                _categoryentity.catName = formData.GetValues("catName").First().ToString();
                _categoryentity.catDescription = formData.GetValues("catDescription").First();
               
                    
                _categoryservice.UpdateCategory(_categoryentity);

                return new APIResponse((int)HttpStatusCode.OK, "Success", _categoryentity);

            }
                return new APIResponse((int)HttpStatusCode.NotFound, "No Record Found with ID " + Id);
            }
            catch (Exception ex)
            {
                return new APIResponse((int)HttpStatusCode.InternalServerError, ex.Message);
            }

        }


    }

}
