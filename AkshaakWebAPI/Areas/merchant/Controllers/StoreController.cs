﻿using System;
using System.Net;
using System.Web.Http;
using BusinessEntities;
using Interfaces;
using VMD.RESTApiResponseWrapper.Net.Wrappers;
using Common;

namespace AkshaakWebAPI.Controllers
{

    public class StoreController : ApiController
    {

        private readonly IStore _storeService;
        private readonly IStoreAddress _storeAdressservice;
        private readonly IStoreTiming _storeTiming;
        private readonly IMerchantBankDetail _merchantBankDetail;

        StoreEntity _storeEntity;
        StoreAddressEntity _storeAddressEntity;
        StoreTimingEntity _storeTimingEntity;

        public StoreController(IStoreAddress storeAdressService ,
                               IStore storeService,
                               IStoreTiming storeTiming,
                               IMerchantBankDetail merchantBankDetail)
        {           
            _storeAdressservice = storeAdressService;
            _storeService = storeService;
            _storeTiming = storeTiming;
            _merchantBankDetail = merchantBankDetail;
        }


        [HttpGet]
        [Route("api/merchant/GetStorebyMerchantID/{Id}")]
        public APIResponse GetStorebyMerchantID(Guid merchantID)
        {
            try
            {
                _storeEntity = _storeService.GetStorebyMerchantID(merchantID);

                if (_storeEntity != null)
                {

                    return new APIResponse((int)HttpStatusCode.OK, Constants.Success_Message, _storeEntity);
                }

                return new APIResponse((int)HttpStatusCode.NotFound, Constants.No_Record_found + merchantID);
            }
            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }
        }


        [HttpGet]
        [Route("api/merchant/GetStorebyStoreID/{StoreId}")]
        public APIResponse GetStorebyStoreID(Guid StoreId)
        {
            try
            {
                _storeEntity = _storeService.GetStorebyStoreID(StoreId);

                if (_storeEntity != null)
                {

                    return new APIResponse((int)HttpStatusCode.OK, Constants.Success_Message , _storeEntity);
                }

                return new APIResponse((int)HttpStatusCode.NotFound, Constants.No_Record_found + StoreId);
            }
            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }
        }

       

        [HttpPost]
        [Route("api/merchant/SaveStore/")]
        public APIResponse SaveStore([FromBody] StoreEntity storeEntity)
        {
            Guid StoreID = Guid.Empty;
            
            try
            {               
                if (storeEntity.Myshop)
                {                    
                     StoreID = _storeService.Save(storeEntity);
                }
                
                else if (storeEntity.lcoationSetup)
                {
                    /// Save Store Address Here 
                    _storeAddressEntity = storeEntity.ObjStoreAddress;
                    _storeAdressservice.Save(_storeAddressEntity);

                    /// Save Store Timings Here 
                    _storeTimingEntity = storeEntity.ObjStoreTiming;                   

                    //// Delete Previous Time
                    /// 
                    foreach (string sId in _storeTimingEntity.stoTimDayName.ToString().Split('^'))
                    {
                        if (sId != "" && sId != "undefined")
                        {
                            _storeTimingEntity.stoTimDayName = sId;
                            _storeTiming.Save(_storeTimingEntity);
                        }
                    }
                }

                else
                {                 
                    _merchantBankDetail.Save(storeEntity.ObjMerchantBankDetail);
                    _storeService.Save(storeEntity);
                }

                return new APIResponse((int)HttpStatusCode.OK, Constants.Success_Message, StoreID);

            }
            catch (Exception ex)
            {
                return new APIResponse((int)HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        public void DeleteStoreTimingbyStoreID(Guid _stoID)
        {
            _storeTiming.Delete(_stoID);
        }

    }

}
