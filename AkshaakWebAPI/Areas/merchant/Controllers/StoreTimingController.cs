﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Web;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Web.Configuration;
using System.Net.Http;
using System.Web.Http;
using BusinessEntities;
using Interfaces;
using VMD.RESTApiResponseWrapper.Net.Wrappers;
using AkshaakWebAPI.Helpers;
using System.Net.Http.Formatting;
using System.Diagnostics;
using System.Text;
using Uploadfiles;

namespace AkshaakWebAPI.Areas.merchant.Controllers
{
    public class StoreTimingController : ApiController
    {

        private readonly IStoreTiming _storeTservice;
      
        public StoreTimingController(IStoreTiming storeTservice)
        {
            _storeTservice = storeTservice;
        }
        
        [Authorize]
        [HttpPost]
        [Route("api/merchant/AddStoreTiming")]
        public APIResponse AddStoreTiming(StoreTimingEntity storetEntity)
        {

            try
            {
                _storeTservice.Save(storetEntity);
                return new APIResponse((int)HttpStatusCode.OK, "Success");

            }
            catch (Exception ex)
            {
                return new APIResponse((int)HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        

       
    }
}
