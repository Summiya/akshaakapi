﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Web;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Web.Configuration;
using System.Net.Http;
using System.Web.Http;
using BusinessEntities;
using Interfaces;
using VMD.RESTApiResponseWrapper.Net.Wrappers;
using AkshaakWebAPI.Helpers;
using System.Net.Http.Formatting;
using System.Diagnostics;
using System.Text;
using Uploadfiles;

namespace AkshaakWebAPI.Areas.merchant.Controllers
{
    public class StoreAddressController : ApiController
    {
        private readonly IStoreAddress _storeAddressService;
        StoreAddressEntity _objstoreadress = new StoreAddressEntity();


        public StoreAddressController(IStoreAddress storeAservice)
        {
            _storeAddressService = storeAservice;
        }

        [HttpPost]
        [Route("api/AddStoreAddress")]
        public APIResponse AddStoreAddress(StoreAddressEntity storeAEntity)
        {

            try
            {
                 _storeAddressService.Save(storeAEntity);
                return new APIResponse((int)HttpStatusCode.OK, "Success");

            }
            catch (Exception ex)
            {
                return new APIResponse((int)HttpStatusCode.InternalServerError, ex.Message);
            }

        }
        


    }
}
