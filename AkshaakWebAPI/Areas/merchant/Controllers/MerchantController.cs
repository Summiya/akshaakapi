﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks


using System.Configuration;
using System.Text;
using System.IO;

using System.Web;
using System.Web.Http;
using BusinessEntities;
using Interfaces;
using VMD.RESTApiResponseWrapper.Net.Wrappers;
using Common;

using Constant;

namespace AkshaakWebAPI.Areas.merchant.Controllers
{
    public class MerchantController : ApiController
    {
        private readonly IMerchant _merchantService;
        private readonly ILogin _loginService;

        MerchantEntity _objmerchant = new MerchantEntity();


        public MerchantController(IMerchant merchantService, ILogin loginservice)
        {
            _merchantService = merchantService;
            _loginService = loginservice;
        }


        [Authorize]
        [Route("api/merchant/Merchant")]
        [HttpGet]
        public APIResponse Get(Guid merID)
        {
            try
            {
                var merchantData = _merchantService.GetRecordByID(merID);
                if (merchantData != null)
                {

                    return new APIResponse(200, "Successs", merchantData);
                }

                return new APIResponse((int)HttpStatusCode.NotFound, "No Record Found with ID " + merID);
                
            }
            catch(Exception e)
            {
                throw new ApiException(e.Message);
            }
            
        }


        [Authorize]
        [HttpGet]
        [Route("api/merchant/GetAllMerchants/")]
        public APIResponse GetAllMerchants(string SearchValue = null, int PageNo = 1, int PageSize = 10, string SortColumn = null, string SortOrder = null)
        {
            try
            {
                var response = _merchantService.GetAllRecords(SearchValue, PageNo, PageSize, SortColumn, SortOrder);
                return new APIResponse((int)HttpStatusCode.OK, "Success", response);
            }
            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/merchant/MerchantEmailExist/{email}")]
        public APIResponse MerchantEmailExist(string email)
        {
            try
            {
                bool IsExist = _merchantService.ISMerchantExist(email);
                return new APIResponse((int)HttpStatusCode.OK, Constants.Success_Message, IsExist);
               
            }
            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }
        }


        [AllowAnonymous]
        [HttpPost]
        [Route("api/merchant/InsertMerchant/")]
        public APIResponse InsertMerchant([FromBody] MerchantEntity _merchantEntity)
        {
            Guid merID = Guid.Empty;

            try
            {
                merID = _merchantService.Save(_merchantEntity);
                
                if (merID != Guid.Empty)
                {
                    /*
                     * Add into login here
                    */

                    LoginEntity loginEntity = new LoginEntity();
                    loginEntity.cusID = merID.ToString();
                    loginEntity.cusLogEmail = _merchantEntity.Email;
                    loginEntity.cusLogType = Constant.Constant.MERCHANT;

                    loginEntity.cusLogIsVerified = false;

                    Guid loginID = _loginService.Save(loginEntity);


                    Dictionary<string, string> _param = new Dictionary<string, string>();

                    _param.Add("Email", loginEntity.cusLogEmail);
                    _param.Add("Password", _merchantEntity.Password);
                    _param.Add("ConfirmPassword", _merchantEntity.Password);

                    string responseString = HttpPostRequest(ConfigurationManager.AppSettings["Register_API_LINK"], "post", _param);

                    return new APIResponse((int)HttpStatusCode.OK, Constants.Success_Message, merID);
                }
                else

                    return new APIResponse((int)HttpStatusCode.BadRequest, Constants.General_Error);
               
            }

            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }
        }

        internal string HttpPostRequest(string url, string method, Dictionary<string, string> postParameters)
        {
            try
            {
                string postData = "";

                foreach (string key in postParameters.Keys)
                {
                    postData += HttpUtility.UrlEncode(key) + "="
                          + HttpUtility.UrlEncode(postParameters[key]) + "&";
                }

                System.Net.ServicePointManager.Expect100Continue = false;
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                myHttpWebRequest.Method = method;

                byte[] data = Encoding.ASCII.GetBytes(postData);

                myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
                myHttpWebRequest.ContentLength = data.Length;

                Stream requestStream = myHttpWebRequest.GetRequestStream();
                requestStream.Write(data, 0, data.Length);
                requestStream.Close();

                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                Stream responseStream = myHttpWebResponse.GetResponseStream();

                StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);

                string pageContent = myStreamReader.ReadToEnd();

                myStreamReader.Close();
                responseStream.Close();

                myHttpWebResponse.Close();

                return pageContent;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
