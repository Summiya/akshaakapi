﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace Uploadfiles
{
     public static class Upload
    {
       
        public static async Task<string> Fileuploading(HttpContent file , string filepath)
        {
            
            var thisFileName = file.Headers.ContentDisposition.FileName.Trim('\"');
            
            if (thisFileName.Length == 0)
            {
                throw new ApiException("File Lenght is Zero");

            }

            if (System.IO.Path.GetExtension(thisFileName.ToLower()).ToString() != ".jpg")
            {
                throw new ApiException("File must be a type of JPG");
            }

            string filename = String.Empty;
            Stream input = await file.ReadAsStreamAsync();
            string directoryName = String.Empty;
            string URL = String.Empty;
            string tempDocUrl = WebConfigurationManager.AppSettings["DocsUrl"];

            //if (formData["ClientDocs"] == "ClientDocs")
            {
                var path = HttpRuntime.AppDomainAppPath;
                directoryName = Path.Combine(path, filepath);
                filename = Path.Combine(directoryName, thisFileName);
                
                //Deletion exists file  
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }

                string DocsPath = tempDocUrl + "/" + filepath + "/";
                URL = DocsPath + thisFileName;

            }


            //Directory.CreateDirectory(@directoryName);  
            using (Stream file_ = File.OpenWrite(filename))
            {
                input.CopyTo(file_);
                //close file  
                file_.Close();
            }
            return thisFileName;
        }
    }
}
