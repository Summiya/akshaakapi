﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBase;
using BusinessEntities;
using AutoMapper;

namespace MapperUtility
{
    public class MapperUtil
    {

        public static void InitializeMapper()
        {

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Category, CategoryEntity>().ReverseMap();
                cfg.CreateMap<Store, StoreEntity>().ReverseMap();

                cfg.CreateMap<Country, CommonEntity>().ReverseMap();
                cfg.CreateMap<City, CityEntity>().ReverseMap();
                cfg.CreateMap<CityArea, CityAreaEntity>().ReverseMap();
                

                cfg.CreateMap<StoreAddress, StoreAddressEntity>().ReverseMap();
                cfg.CreateMap<StoreTiming, StoreTimingEntity>().ReverseMap();

                cfg.CreateMap<Login, LoginEntity>().ReverseMap();
            });
        }
    }
}
