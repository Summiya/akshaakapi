﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Text;
using SpaceAgencyCMS.Common;

/// <summary>
/// Summary description for SmsClass
/// </summary>
public class SmsClass
{
    public static string SendSMS(string Mobile_Number, string Message)
    {
        string output = Mobile_Number.Replace("+", "00");
        string sNumber = output;
        string sMessage = Message;
        string sUser = Constants.SMS_USER;
        string sPass = Constants.SMS_PASSWORD;
        string sSenderID = Constants.SMS_SENDER_ID;
        string sDatenew = DateTime.Now.ToString("yyyy-MM-ddHH:mm:ss");
        string sDate = HttpUtility.UrlEncode(sDatenew);
        string sURL = Constants.SMS_URL + sUser + "&password=" + sPass + "&senderid=" + sSenderID + "&to=" + sNumber + "&text=" + sMessage + "&type=text&datetime=" + sDate + "";
        string sResponse = GetResponse(sURL);
        return sResponse;
    }

    private static string GetResponse(string sURL)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURL);
        //request.Method = "GET";
        request.MaximumAutomaticRedirections = 4;
        request.Credentials = CredentialCache.DefaultCredentials;
        try
        {
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream receiveStream = response.GetResponseStream(
            );
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
            string sResponse = readStream.ReadToEnd();
            response.Close();
            readStream.Close();
            return sResponse;
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
}