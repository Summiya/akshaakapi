﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.Interfaces;


namespace UnitOfWork
{
   public interface IUnitOfWork
    {
        /// <summary>
        /// Save method.
        /// </summary>
        void Save();

        IMerchantRepository MerchantRepository { get; }
    }
}
