﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenericRepository;
using System.Data;
using System.Diagnostics;
using System.Data.Entity.Validation;
using DataBase;
using Repository.Interfaces;
using Repository;

namespace UnitOfWork
{
    /// <summary>
    /// Unit of Work class responsible for DB transactions
    /// </summary>
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        
        #region Private member variables...

        private Repository<Category> _categoryRepository;
        private Repository<Store> _storeRepository;

        private Repository<Country> _countryRepository;
        private Repository<City> _cityRepository;
        private Repository<CityArea> _cityareaRepository;

        private Repository<StoreAddress> _storeaddressRepository;
        private Repository<StoreTiming> _storetimingRepository;

       

        private AkShaakEntitiesDB _context = null;
      
        //private Repository<Role> _roleRepository;
        #endregion

        public UnitOfWork()
        {
            _context = new AkShaakEntitiesDB();
        }

        #region Public Repository Creation properties...

        /// <summary>
        /// Get/Set Property for product repository.
        /// </summary>
        /// 
        /// <summary>
        /// Get/Set Property for user repository.
        /// </summary>
        public Repository<Category> CategoryRepository
        {
            get
            {
                if (this._categoryRepository == null)
                    this._categoryRepository = new Repository<Category>(_context);
                return _categoryRepository;
            }
        }


        /// <summary>
        /// Get/Set Property for product repository.
        /// </summary>
        /// 
        /// <summary>
        /// Get/Set Property for user repository.
        /// </summary>
        public Repository<Store> StoreRepository
        {
            get
            {
                if (this._storeRepository == null)
                    this._storeRepository = new Repository<Store>(_context);
                return _storeRepository;
            }
        }


        /// <summary>
        /// Get/Set Property for product repository.
        /// </summary>
        /// 
        /// <summary>
        /// Get/Set Property for user repository.
        /// </summary>
        public Repository<Country> CountryRepository
        {
            get
            {
                if (this._countryRepository == null)
                    this._countryRepository = new Repository<Country>(_context);
                return _countryRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for product repository.
        /// </summary>
        /// 
        /// <summary>
        /// Get/Set Property for user repository.
        /// </summary>
        public Repository<City> CityRepository
        {
            get
            {
                if (this._cityRepository == null)
                    this._cityRepository = new Repository<City>(_context);
                return _cityRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for product repository.
        /// </summary>
        /// 
        /// <summary>
        /// Get/Set Property for user repository.
        /// </summary>
        public Repository<CityArea> CityAreaRepository
        {
            get
            {
                if (this._cityareaRepository == null)
                    this._cityareaRepository = new Repository<CityArea>(_context);
                return _cityareaRepository;
            }
        }


        /// <summary>
        /// Get/Set Property for product repository.
        /// </summary>
        /// 
        /// <summary>
        /// Get/Set Property for user repository.
        /// </summary>
        public Repository<StoreTiming> StoreTimingRepository
        {
            get
            {
                if (this._storetimingRepository == null)
                    this._storetimingRepository = new Repository<StoreTiming>(_context);
                return _storetimingRepository;
            }
        }


        /// <summary>
        /// Get/Set Property for product repository.
        /// </summary>
        /// 
        /// <summary>
        /// Get/Set Property for user repository.
        /// </summary>
        public Repository<StoreAddress> StoreAddressRepository
        {
            get
            {
                if (this._storeaddressRepository == null)
                    this._storeaddressRepository = new Repository<StoreAddress>(_context);
                return _storeaddressRepository;
            }
        }

        
    
        private IMerchantRepository _merchantRepository;
        public IMerchantRepository MerchantRepository
        {
            get
            {
                if (_merchantRepository == null)
                {
                    _merchantRepository = new MerchantRepository(_context);
                }
                return _merchantRepository;
            }
        }




        /// <summary>
        /// Get/Set Property for product repository.
        /// </summary>
        /// 
        /// <summary>
        /// Get/Set Property for user repository.
        /// </summary>
        private Repository<MerchantBankDetail> _merchantBankDetailRepository;
        public Repository<MerchantBankDetail> MerchantBankDetailRepository
        {
            get
            {
                if (this._merchantBankDetailRepository == null)
                    this._merchantBankDetailRepository = new Repository<MerchantBankDetail>(_context);
                return _merchantBankDetailRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for user repository.
        /// </summary>
        private Repository<Login> _loginRepository;
        public Repository<Login> LoginRepository
        {
            get
            {
                if (this._loginRepository == null)
                    this._loginRepository = new Repository<Login>(_context);
                return _loginRepository;
            }
        }


        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                var outputLines = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.Add(string.Format("{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:", DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                System.IO.File.AppendAllLines(@"D:\ASP.NET\ASP.NET PROJECTS\REPOSITORY\REPOSITORY\errors.txt", outputLines);

                throw e;
            }

        }

        #endregion

        #region Implementing IDiosposable...

        #region private dispose variable declaration...
        private bool disposed = false;
        #endregion

        /// <summary>
        /// Protected Virtual Dispose method
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}