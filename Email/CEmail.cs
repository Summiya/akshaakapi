﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;

namespace Email
{
    public class CEmail
    {
        
        public void SendMailMessage(string toEmail, string fromEmail, string bcc, string cc, string subject, string body, List<string> attachmentFullPath)
        {
            //create the MailMessage object
            MailMessage mMailMessage = new MailMessage();

            //set the sender address of the mail message
            if (!string.IsNullOrEmpty(fromEmail))
            {
                mMailMessage.From = new MailAddress(fromEmail);
            }
            else
            {
                mMailMessage.From =  new MailAddress(ConfigurationManager.AppSettings["Smtp_SenderEmail"].ToString());
            }

            //set the recipient address of the mail message
            mMailMessage.To.Add(new MailAddress(toEmail));

            //set the blind carbon copy address
            if (!string.IsNullOrEmpty(bcc))
            {
                mMailMessage.Bcc.Add(new MailAddress(bcc));
            }

            //set the carbon copy address
            if (!string.IsNullOrEmpty(cc))
            {
                mMailMessage.CC.Add(new MailAddress(cc));
            }

            //set the subject of the mail message
            if (!string.IsNullOrEmpty(subject))
            {
                mMailMessage.Subject = "Web Application Notification";
            }
            else
            {
                mMailMessage.Subject = subject;
            }

            //set the body of the mail message
            mMailMessage.Body = body;

            //set the format of the mail message body
            mMailMessage.IsBodyHtml = false;

            //set the priority
            mMailMessage.Priority = MailPriority.Normal;

            //add any attachments from the filesystem
            foreach (var attachmentPath in attachmentFullPath)
            {
                Attachment mailAttachment = new Attachment(attachmentPath);
                mMailMessage.Attachments.Add(mailAttachment);
            }

            //create the SmtpClient instance
            SmtpClient mSmtpClient = new SmtpClient(ConfigurationManager.AppSettings["smtp_host"].ToString());


            mSmtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtp_port"]);
            mSmtpClient.UseDefaultCredentials = false;
            mSmtpClient.EnableSsl = true;
            mSmtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["smtp_email_acc"].ToString(), ConfigurationManager.AppSettings["smtp_email_acc_pswrd"].ToString());
            
            //send the mail message
            mSmtpClient.Send(mMailMessage);
        }
        
    }
}